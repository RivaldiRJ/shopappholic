package com.shift.fashion;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.myjson.Gson;
import com.shift.fashion.adapter.ShopListAdapter;
import com.shift.fashion.api.NearByApi;
import com.shift.fashion.api.SearchApi;
import com.shift.fashion.api.SearchApi.ApiDao;
import com.shift.fashion.api.SearchApi.ApiResultListener;
import com.shift.fashion.dao.FactoryDao;
import com.shift.fashion.utils.Constants;

public class SearchActivity extends SherlockActivity {
	private SearchApi searchApi;
	NearByApi nearApi;
	int requestCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		requestCode = getIntent().getExtras().getInt("requestCode");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		final EditText txtSearch = (EditText) findViewById(R.id.editTextSearch);
		final ImageButton buttonSearch = (ImageButton) findViewById(R.id.imageButtonSearch);
		final ListView listSearch = (ListView) findViewById(R.id.listViewSearch);
		final ProgressBar progressBarSearch = (ProgressBar) findViewById(R.id.progressBarSearch);

		nearApi = new NearByApi(this, new NearByApi.ApiResultListener() {

			@Override
			public void onApiResultError(String errorMessage) {
				// TODO Auto-generated method stub
				progressBarSearch.setVisibility(View.GONE);

			}

			@Override
			public void onApiPreCall() {
				// TODO Auto-generated method stub
				progressBarSearch.setVisibility(View.VISIBLE);

			}

			@Override
			public void onApiResultOk(
					com.shift.fashion.api.NearByApi.ApiDao data) {
				// TODO Auto-generated method stub
				progressBarSearch.setVisibility(View.GONE);
				final ShopListAdapter adapter = new ShopListAdapter(
						getApplicationContext(), data.getResult());
				listSearch.setAdapter(adapter);
				final List<FactoryDao> dataFactory = data.getResult();
				listSearch.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						if (requestCode == ReviewActivity.ACTION_REQUEST_FACTORY) {
							final Gson gson = new Gson();
							Intent intent = new Intent();
							intent.putExtra(Constants.Pref.SHOP_OBJECT,
									gson.toJson(dataFactory.get(arg2)));
							setResult(ReviewActivity.ACTION_REQUEST_FACTORY,
									intent);
							finish();
						} else {
							DetailedActivity.startThisActivity(
									dataFactory.get(arg2), SearchActivity.this);
						}
					}
				});

			}
		});
		nearApi.callApi();
		searchApi = new SearchApi(this, new ApiResultListener() {

			@Override
			public void onApiPreCall() {
				// TODO Auto-generated method stub
				progressBarSearch.setVisibility(View.VISIBLE);
			}

			@Override
			public void onApiResultOk(final ApiDao data) {
				// beres
				progressBarSearch.setVisibility(View.GONE);

				final ShopListAdapter adapter = new ShopListAdapter(
						getApplicationContext(), data.getResult().getStore());
				listSearch.setAdapter(adapter);
				final List<FactoryDao> dataFactory = data.getResult()
						.getStore();
				listSearch.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						if (requestCode == ReviewActivity.ACTION_REQUEST_FACTORY) {
							final Gson gson = new Gson();
							Intent intent = new Intent();
							intent.putExtra(Constants.Pref.SHOP_OBJECT,
									gson.toJson(dataFactory.get(arg2)));
							setResult(ReviewActivity.ACTION_REQUEST_FACTORY,
									intent);
							finish();
						} else {
							DetailedActivity.startThisActivity(
									dataFactory.get(arg2), SearchActivity.this);
						}
					}
				});
			}

			@Override
			public void onApiResultError(String errorMessage) {
				// TODO Auto-generated method stub
				progressBarSearch.setVisibility(View.GONE);
				Toast.makeText(getApplicationContext(), errorMessage,
						Toast.LENGTH_SHORT).show();
			}
		});
		buttonSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				searchApi.callApi(txtSearch.getText() + "");
			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	public static void startThisActivity(Activity ctx, int requestCode) {
		Intent intent = new Intent(ctx, SearchActivity.class);
		intent.putExtra("requestCode", requestCode);
		ctx.startActivityForResult(intent, requestCode);
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

}
