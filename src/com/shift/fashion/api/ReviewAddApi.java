package com.shift.fashion.api;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.ReviewDao;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.MultiPartPart;

public class ReviewAddApi extends BaseApi {
	private String URL = Constants.APIUrl.REVIEW_ADD;

	private TheAsyncTask theAsyncTask;

	public void cancelAPI() {
		try {
			theAsyncTask.cancel(true);
		} catch (Exception e) {

		}
	}

	private ApiResultListener resultListener = new ApiResultListener() {

		@Override
		public void onApiResultError(String errorMessage) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiPreCall() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiResultOk(ApiDao data) {
			// TODO Auto-generated method stub

		}
	};

	String id_toko, comment, parent, userid, token;
	Bitmap bitmap;
	float rating;

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}

	public void callAPI(String id_toko, String comment, String parent,
			String userid, String token, Bitmap bitmap, float rating) {
		this.bitmap = bitmap;
		this.id_toko = id_toko;
		this.comment = comment;
		this.parent = parent;
		this.rating = rating;
		this.userid = userid;
		this.token = token;
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();

	}

	public class ApiDao extends BaseApiDao {
		private ReviewDao result;

		public ReviewDao getResult() {
			return result;
		}
	}

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {

		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
		ArrayList<MultiPartPart> dataPart = new ArrayList<MultiPartPart>();

		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
			param.add(new BasicNameValuePair("userid", userid));
			param.add(new BasicNameValuePair("id_toko", id_toko));
			param.add(new BasicNameValuePair("comment", comment));
			param.add(new BasicNameValuePair("parent", parent));
			param.add(new BasicNameValuePair("token", token));
			param.add(new BasicNameValuePair("rating", "" + rating));
			if(bitmap!=null){
				dataPart.add(new MultiPartPart("image", bitmap, userid));
			}
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			String jsonText = "";
			try {
				jsonText = ConnectionTools.sendMultiPartForm(context, URL,
						param, dataPart);
				// ArrayList<NameValuePair> data = new
				// ArrayList<NameValuePair>();
				// data.add(new BasicNameValuePair("user", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// String jsonText = ConnectionTools.sendHTTPPOST(URL, data);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				Gson gson = new Gson();
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new BaseApiDao("error", e.getMessage());
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	public ReviewAddApi(Context ctx, ApiResultListener listener) {
		super(ctx);
		this.resultListener = listener;
		// TODO Auto-generated constructor stub
	}

}
