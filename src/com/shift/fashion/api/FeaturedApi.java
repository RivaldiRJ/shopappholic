package com.shift.fashion.api;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.FactoryDao;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.Utils;

public class FeaturedApi extends BaseApi {
	private String URL = Constants.APIUrl.FEATURED;

	private TheAsyncTask theAsyncTask;

	private ApiResultListener resultListener;

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}

	public void callApi() {
		URL = URL
 + "?apikey=b26818d947f90a0553e1b17530655c9f&id=6&lat="
				+ Utils.getMyLatitude(context) + "&lng="
				+ Utils.getMyLongitude(context) + "";
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();
	}

	public FeaturedApi(Context ctx, ApiResultListener listener) {
		super(ctx);
		this.resultListener = listener;
		// TODO Auto-generated constructor stub
	}

	public void setResultListener(ApiResultListener resultListener) {
		this.resultListener = resultListener;
	}

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {
		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			try {
				String jsonText = ConnectionTools.sendHttpGet(URL);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				Gson gson = new Gson();
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
			} catch (Exception e) {
				return new BaseApiDao("error", e.getMessage());
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	private class ApiDao extends BaseApiDao {
		private Result result;

		/**
		 * @return the result
		 */
		public Result getResult() {
			return result;
		}

		private class Result {
			List<FactoryDao> result1;
			List<FactoryDao> result2;

			/**
			 * @return the restaurants1
			 */
			public List<FactoryDao> getRestaurants1() {
				return result1;
			}

			/**
			 * @return the restaurants2
			 */
			public List<FactoryDao> getRestaurants2() {
				return result2;
			}

		}
	}
}
