package com.shift.fashion.api;

import android.content.Context;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.DetailedFactoryDao;
import com.shift.fashion.utils.CacheManager;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.Utils;

public class DetailedFactoryApi extends BaseApi {
	public DetailedFactoryApi(Context ctx, ApiResultListener listener) {
		super(ctx);
		this.resultListener = listener;
		// TODO Auto-generated constructor stub
	}

	private String id_factory;
	private String URL = Constants.APIUrl.DETAIL;

	public void cancelAPI() {
		try {
			theAsyncTask.cancel(true);
		} catch (Exception e) {

		}
	}

	public void callApi(String id_factory) {
		this.id_factory = id_factory;
		URL = URL + "?apikey=b26818d947f90a0553e1b17530655c9f&id=" + id_factory
				+ "&lat=" + Utils.getMyLatitude(context) + "&lng="
				+ Utils.getMyLongitude(context) + "";
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();
	}

	private TheAsyncTask theAsyncTask;

	private ApiResultListener resultListener;

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {
		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			final Gson gson = new Gson();

			try {
				String jsonText = ConnectionTools.sendHttpGet(URL);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
				CacheManager.writeAllCachedText(context, "Detailed_Factory "
						+ id_factory, jsonText);

			} catch (Exception e) {
				String jsonCache = "";
				try {
					jsonCache = CacheManager.readAllCachedText(context,
							"Detailed_Factory " + id_factory);
				} catch (Exception ex) {

				}
				if (jsonCache != null && !jsonCache.equals("")) {
					apiDao = gson.fromJson(jsonCache, ApiDao.class);
				} else {
					return new BaseApiDao("error", e.getMessage());
				}
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}

	public class ApiDao extends BaseApiDao {
		private DetailedFactoryDao result;

		public DetailedFactoryDao getResult() {
			return result;
		}
	}
}
