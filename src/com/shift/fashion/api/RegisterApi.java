package com.shift.fashion.api;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.UserDao;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.MultiPartPart;

public class RegisterApi extends BaseApi {
	private String URL = Constants.APIUrl.REGISTER;

	private TheAsyncTask theAsyncTask;

	public void cancelAPI() {
		try {
			theAsyncTask.cancel(true);
		} catch (Exception e) {

		}
	}

	private ApiResultListener resultListener = new ApiResultListener() {

		@Override
		public void onApiResultError(String errorMessage) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiPreCall() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiResultOk(ApiDao data) {
			// TODO Auto-generated method stub

		}
	};

	String username;

	String password;

	String email;

	String nama_asli;

	String no_phone;

	String birthday;

	String url;

	String about;

	String sex;

	Bitmap bitmap;

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}

	public void callAPI(String username, String password, String email,
			String nama_asli, String no_phone, String birthday, String url,
			String about, String sex, Bitmap bitmap) {
		this.about = about;
		this.username = username;
		this.birthday = birthday;
		this.bitmap = bitmap;
		this.email = email;
		this.nama_asli = nama_asli;
		this.no_phone = no_phone;
		this.password = password;
		this.sex = sex;
		this.url = url;
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();

	}

	public class ApiDao extends BaseApiDao {
		private UserDao result;

		public UserDao getResult() {
			return result;
		}
	}

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {

		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();
		ArrayList<MultiPartPart> dataPart = new ArrayList<MultiPartPart>();

		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
			param.add(new BasicNameValuePair("user", username));
			param.add(new BasicNameValuePair("password", password));
			param.add(new BasicNameValuePair("email", email));
			param.add(new BasicNameValuePair("nama_asli", nama_asli));
			param.add(new BasicNameValuePair("no_phone", no_phone));
			param.add(new BasicNameValuePair("birthday", birthday));
			param.add(new BasicNameValuePair("url", url));
			param.add(new BasicNameValuePair("about", about));
			param.add(new BasicNameValuePair("sex", sex));
			if (bitmap != null) {
				dataPart.add(new MultiPartPart("image", bitmap, username));
			}
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			try {
				String jsonText = ConnectionTools.sendMultiPartForm(context,
						URL, param, dataPart);
				// ArrayList<NameValuePair> data = new
				// ArrayList<NameValuePair>();
				// data.add(new BasicNameValuePair("user", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// data.add(new BasicNameValuePair("", ""));
				// String jsonText = ConnectionTools.sendHTTPPOST(URL, data);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				Gson gson = new Gson();
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
			} catch (Exception e) {
				return new BaseApiDao("error", e.getMessage());
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	public RegisterApi(Context ctx, ApiResultListener listener) {
		super(ctx);
		this.resultListener = listener;
		// TODO Auto-generated constructor stub
	}

}
