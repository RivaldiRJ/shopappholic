package com.shift.fashion.api;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.UserDao;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;

public class LoginApi extends BaseApi {
	private String URL = Constants.APIUrl.LOGIN;

	private TheAsyncTask theAsyncTask;
	private String username;
	private String password;

	private ApiResultListener resultListener = new ApiResultListener() {

		@Override
		public void onApiResultError(String errorMessage) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiPreCall() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiResultOk(ApiDao data) {
			// TODO Auto-generated method stub

		}
	};

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}

	public LoginApi(Context ctx, ApiResultListener listener) {
		super(ctx);
		this.resultListener = listener;
		// TODO Auto-generated constructor stub
	}

	public class ApiDao extends BaseApiDao {
		private UserDao result;

		public UserDao getResult() {
			return result;
		}
	}

	public void callApi(String username, String password) {
		// "?apikey=b26818d947f90a0553e1b17530655c9f&id=6&lat=-6.948494&lng=107.645949";
		this.username = username;
		this.password = password;
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();
	}

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {
		ArrayList<NameValuePair> data = new ArrayList<NameValuePair>();

		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
			data.add(new BasicNameValuePair("username", username));
			data.add(new BasicNameValuePair("password", password));
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			try {
				String jsonText = ConnectionTools.sendHTTPPOST(URL, data);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				Gson gson = new Gson();
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
			} catch (Exception e) {
				return new BaseApiDao("error", e.getMessage());
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

}
