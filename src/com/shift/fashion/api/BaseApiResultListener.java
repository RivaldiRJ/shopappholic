package com.shift.fashion.api;


public interface BaseApiResultListener {
	public void onApiPreCall();
	public void onApiResultError(String errorMessage);
}
