package com.shift.fashion.api;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.FactoryDao;
import com.shift.fashion.utils.CacheManager;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.Utils;

public class NearByApi extends BaseApi {
	private String URL = Constants.APIUrl.NEAR;

	private TheAsyncTask theAsyncTask;
	final Gson gson = new Gson();
	
	private ApiResultListener resultListener = new ApiResultListener() {

		@Override
		public void onApiResultError(String errorMessage) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiPreCall() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiResultOk(ApiDao data) {
			// TODO Auto-generated method stub

		}
	};

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}
	public void callApi(){
		URL = URL + "?apikey=b26818d947f90a0553e1b17530655c9f&id=6&lat="
				+ Utils.getMyLatitude(context) + "&lng="
				+ Utils.getMyLongitude(context) + "";
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();
	}
	public NearByApi(Context ctx, ApiResultListener listener) {
		super(ctx);
		// TODO Auto-generated constructor stub
		this.resultListener = listener;
	}

	public void setResultListener(ApiResultListener resultListener) {
		this.resultListener = resultListener;
	}

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {
		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			try {
				String jsonText = ConnectionTools.sendHttpGet(URL);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
				CacheManager.writeAllCachedText(context, "Category", jsonText);
			} catch (Exception e) {
				final String jsonCache = CacheManager.readAllCachedText(context, "Category");
				if(jsonCache!=null&&!jsonCache.equals("")){
					apiDao = gson.fromJson(jsonCache, ApiDao.class);
				}else{
					return new BaseApiDao("error", e.getMessage());
				}
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	public class ApiDao extends BaseApiDao {
		private List<FactoryDao> result;

		public List<FactoryDao> getResult() {
			return result;
		}
	}

}
