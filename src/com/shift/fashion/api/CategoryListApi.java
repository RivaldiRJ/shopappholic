package com.shift.fashion.api;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.FactoryDao;
import com.shift.fashion.utils.CacheManager;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.Utils;

public class CategoryListApi extends BaseApi {
	private String URL = Constants.APIUrl.FEATURED_CATEGORY;
	private String category;
	private TheAsyncTask theAsyncTask;

	private ApiResultListener resultListener;

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);

		@Override
		public void onApiResultError(String errorMessage);
	}

	public void callApi(String category) {
		this.category = category;
		URL = URL + "?category=" + category.replace(" ", "%20")
				+ "&apikey=b26818d947f90a0553e1b17530655c9f&id=2&lat="
				+ Utils.getMyLatitude(context) + "&lng="
				+ Utils.getMyLongitude(context) + "";
		// "?apikey=b26818d947f90a0553e1b17530655c9f&id=6&lat=-6.948494&lng=107.645949";
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();
	}

	public CategoryListApi(Context ctx, ApiResultListener listener) {
		super(ctx);
		this.resultListener = listener;
		// TODO Auto-generated constructor stub
	}

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {
		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			final Gson gson = new Gson();

			try {
				String jsonText = ConnectionTools.sendHttpGet(URL);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
				CacheManager.writeAllCachedText(context, "Featured_Category"
						+ category, jsonText);
			} catch (Exception e) {
				String jsonCache = "";
				try {
					jsonCache = CacheManager.readAllCachedText(context,
							"Featured_Category" + category);
				} catch (Exception ex) {

				}
				if (jsonCache != null && !jsonCache.equals("")) {
					apiDao = gson.fromJson(jsonCache, ApiDao.class);
				} else {
					return new BaseApiDao("error", e.getMessage());
				}
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	public class ApiDao extends BaseApiDao {
		private Result result;

		/**
		 * @return the result
		 */
		public Result getResult() {
			return result;
		}

		public class Result {
			List<FactoryDao> shopappholic;
			boolean no_featured;

			public boolean isNo_featured() {
				return no_featured;
			}

			public List<FactoryDao> getShopappholic() {
				return shopappholic;
			}
		}
	}

}
