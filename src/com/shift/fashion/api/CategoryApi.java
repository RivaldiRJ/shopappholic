package com.shift.fashion.api;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.utils.CacheManager;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;

public class CategoryApi extends BaseApi {
	private ApiResultListener resultListener;
	private String URL = Constants.APIUrl.CATEGORY;
	private TheAsyncTask theAsyncTask;
	final Gson gson = new Gson();

	public void callApi() {
		// URL = URL+
		// "?apikey=b26818d947f90a0553e1b17530655c9f&id=6&lat=-6.948494&lng=107.645949";
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();
	}

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}

	public CategoryApi(Context ctx, ApiResultListener resultListener) {
		super(ctx);
		this.resultListener = resultListener;
		// TODO Auto-generated constructor stub
	}

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {
		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			try {
				final String jsonText = ConnectionTools.sendHttpGet(URL);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				apiDao = gson.fromJson(jsonText, ApiDao.class);
				CacheManager.writeAllCachedText(context, "Category", jsonText);

			} catch (Exception e) {
				final String jsonCache = CacheManager.readAllCachedText(context, "Category");
				if(jsonCache!=null&&!jsonCache.equals("")){
					apiDao = gson.fromJson(jsonCache, ApiDao.class);
				}else{
					return new BaseApiDao("error", e.getMessage());
				}
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	public class ApiDao extends BaseApiDao {
		private List<Result> result;

		/**
		 * @return the result
		 */
		public List<Result> getResult() {
			return result;
		}

		public class Result {
			String category;

			public String getCategory() {
				return category;
			}
		}
	}

}
