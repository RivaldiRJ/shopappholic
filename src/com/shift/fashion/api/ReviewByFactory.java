package com.shift.fashion.api;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.google.myjson.Gson;
import com.shift.fashion.dao.BaseApiDao;
import com.shift.fashion.dao.ReviewDao;
import com.shift.fashion.utils.CacheManager;
import com.shift.fashion.utils.ConnectionTools;
import com.shift.fashion.utils.Constants;

public class ReviewByFactory extends BaseApi {
	public ReviewByFactory(Context ctx, ApiResultListener listener) {
		super(ctx);
		// TODO Auto-generated constructor stub
		this.resultListener = listener;
	}

	private String URL = Constants.APIUrl.REVIEW_FACTORY;

	public void callApi(String id_factory) {
		URL = URL + "?apikey=b26818d947f90a0553e1b17530655c9f&id=" + id_factory
				+ "&lat=-6.948494&lng=107.645949";
		if (theAsyncTask != null)
			theAsyncTask.cancel(true);
		theAsyncTask = new TheAsyncTask();
		theAsyncTask.execute();
	}
	public void cancelAPI() {
		try{
			theAsyncTask.cancel(true);			
		}catch(Exception e){
			
		}
	}
	private TheAsyncTask theAsyncTask;
	final Gson gson = new Gson();

	private class TheAsyncTask extends AsyncTask<String, Void, BaseApiDao> {
		@Override
		protected void onPreExecute() {
			resultListener.onApiPreCall();
		}

		@Override
		protected BaseApiDao doInBackground(String... params) {
			BaseApiDao apiDao;
			try {
				String jsonText = ConnectionTools.sendHttpGet(URL);
				// CacheManager.writeAllCachedText(context, "test", jsonText);
				// CacheManager.readAllFileText("test");
				apiDao = gson.fromJson((jsonText).toString(), ApiDao.class);
				CacheManager.writeAllCachedText(context, "Category", jsonText);

			} catch (Exception e) {
				final String jsonCache = CacheManager.readAllCachedText(
						context, "Featured_Category");
				if (jsonCache != null && !jsonCache.equals("")) {
					apiDao = gson.fromJson(jsonCache, ApiDao.class);
				} else {
					return new BaseApiDao("error", e.getMessage());
				}
			}
			return apiDao;
		}

		@Override
		protected void onPostExecute(BaseApiDao result) {
			if (result != null && !result.getStatus().equalsIgnoreCase("error")) {
				ApiDao apiDao = (ApiDao) result;
				resultListener.onApiResultOk(apiDao);
			} else
				resultListener.onApiResultError(result.getDescription());
		}

	}

	private ApiResultListener resultListener = new ApiResultListener() {

		@Override
		public void onApiResultError(String errorMessage) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiPreCall() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onApiResultOk(ApiDao data) {
			// TODO Auto-generated method stub

		}
	};

	public interface ApiResultListener extends BaseApiResultListener {
		public void onApiResultOk(ApiDao data);
	}

	public class ApiDao extends BaseApiDao {
		private List<ReviewDao> result;

		public List<ReviewDao> getResult() {
			return result;
		}
	}

}
