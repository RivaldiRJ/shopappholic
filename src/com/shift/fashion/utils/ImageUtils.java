package com.shift.fashion.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import eu.janmuller.android.simplecropimage.CropImage;

public class ImageUtils {

	public static final String TAG = ImageUtils.class.getCanonicalName();

	public static void startCropImage(Activity activity, String filepath,
			int reqCode) {
		Intent intent = new Intent(activity, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, filepath);
		intent.putExtra(CropImage.SCALE, true);

		intent.putExtra(CropImage.ASPECT_X, 1);
		intent.putExtra(CropImage.ASPECT_Y, 1);

		activity.startActivityForResult(intent, reqCode);
	}

	private static void writeImageFile(Bitmap bitmap, File file)
			throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		// Converts the bitmap to JPEG format
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] data = baos.toByteArray();

		// Write the data to File
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(data);
		fos.close();
	}

	public static String getRealPath(Activity activity, Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = activity
				.managedQuery(uri, projection, null, null, null);
		activity.startManagingCursor(cursor);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public static String getRealPasth(ContentResolver cr, Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = cr.query(contentUri, proj, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	// public static Bitmap mark(final Bitmap src, final Context ctx) {
	// int w = src.getWidth();
	// int h = src.getHeight();
	// Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
	// Canvas canvas = new Canvas(result);
	// canvas.drawBitmap(src, 0, 0, null);
	// Paint paint = new Paint();
	// paint.setColor(Color.RED);
	// paint.setAntiAlias(true);
	// Paint paint1 = new Paint();
	// paint1.setColor(Color.RED);
	// paint1.setAntiAlias(true);
	// paint1.setTextSize(30);
	// // paint1.setUnderlineText(true);
	// paint1.setAlpha(150);
	// Calendar c = Calendar.getInstance();
	// // System.out.println("Current time => " + c.getTime());
	//
	// SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	// String formattedDate = df.format(c.getTime());
	//
	// paint1.setFlags(Paint.ANTI_ALIAS_FLAG);
	// canvas.drawText("Date : ", 0, 0, paint);
	// canvas.drawBitmap(BitmapFactory.decodeResource(ctx.getResources(),
	// R.drawable.ic_marker), 0, src.getHeight()
	// - ((src.getHeight() / 100)*25), paint);
	// return result;
	// // return mark(result, "hay ", new Point(0, 0), 150, 18, false);
	// }

	public static Bitmap mark(Bitmap src, String watermark, Point location,
			int alpha, int size, boolean underline) {
		int w = src.getWidth();
		int h = src.getHeight();
		Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

		Canvas canvas = new Canvas(result);
		canvas.drawBitmap(src, 0, 0, null);

		Paint paint = new Paint();
		paint.setColor(Color.YELLOW);
		paint.setAlpha(alpha);
		paint.setTextSize(size);
		paint.setAntiAlias(true);
		paint.setUnderlineText(underline);
		paint.setFakeBoldText(true);
		canvas.drawText(watermark, location.x, location.y, paint);

		return result;
	}
}
