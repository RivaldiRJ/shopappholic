package com.shift.fashion.utils;

import android.graphics.Bitmap;

public class MultiPartPart {
	String paramName;
	Bitmap bitmap;
	String FileName;

	public MultiPartPart(String paramName, Bitmap bitmap, String FileName) {
		// TODO Auto-generated constructor stub
		this.bitmap = bitmap;
		this.paramName = paramName;
		this.FileName = FileName;
	}

	public String getFileName() {
		return FileName;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public String getParamName() {
		return paramName;
	}
}
