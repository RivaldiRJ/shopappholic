/**
 * Copyright 2013 GITS Indonesia, PT.
 */
package com.shift.fashion.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * @edited <a href="rohimatdeni@gmail.com">Deni Rohimat</a>
 * @author Ibnu Sina Wardy
 * 
 */
public class AviaryHelper {
	public static final int ACTION_REQUEST_FEATHER = 100;
	private static final int IMAGE_SIZE = 1000;

	Bitmap mBitmap;

	String mOutputFilePath;
	private static final String FOLDER_NAME = "HospitalPix";
	boolean landscape = false;
	private File mGalleryFolder;

	ImageView mImage;
	File fileTemp;
	Activity activity;
	Fragment fragment = null;
	Uri mImageUri;

	public AviaryHelper(Fragment fragment, ImageView iv) {
		this.fragment = fragment;
		init(fragment.getActivity(), iv);
	}

	public AviaryHelper(Activity ctx, ImageView iv) {
		init(ctx, iv);
	}

	private void init(Activity ctx, ImageView iv) {
		this.mImage = iv;
		this.activity = ctx;

		mGalleryFolder = createFolders();
		fileTemp = new File(mGalleryFolder, "temp");
		mImageUri = Uri.fromFile(fileTemp);
	}

	public Uri getImageUri() {
		return mImageUri;
	}

	public void copyImageToTemp(Context ctx, Uri uri) {

		try {
			InputStream inputStream = ctx.getContentResolver().openInputStream(
					uri);
			OutputStream fOut = null;
			try {
				fOut = new FileOutputStream(fileTemp);
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}
			Utils.CopyStream(inputStream, fOut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	public void clearBitmap() {
		mBitmap = null;
	}

	/**
	 * Try to create the required folder on the sdcard where images will be
	 * saved to.
	 * 
	 * @return
	 */

	private File createFolders() {
		File baseDir;

		if (android.os.Build.VERSION.SDK_INT < 8) {
			baseDir = Environment.getExternalStorageDirectory();
		} else {
			baseDir = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		}

		if (baseDir == null)
			return Environment.getExternalStorageDirectory();

		File aviaryFolder = new File(baseDir, FOLDER_NAME);

		if (aviaryFolder.exists())
			return aviaryFolder;
		if (aviaryFolder.mkdirs())
			return aviaryFolder;

		return Environment.getExternalStorageDirectory();
	}

	public void loadAsync(final Uri uri) {
		Drawable toRecycle = mImage.getDrawable();
		if (toRecycle != null && toRecycle instanceof BitmapDrawable) {
			if (((BitmapDrawable) mImage.getDrawable()).getBitmap() != null)
				((BitmapDrawable) mImage.getDrawable()).getBitmap().recycle();
		}
		mImage.setImageDrawable(null);
		mImageUri = null;

		DownloadAsync task = new DownloadAsync();
		task.execute(uri);
	}

	class DownloadAsync extends AsyncTask<Uri, Void, Bitmap> implements
			OnCancelListener {

		ProgressDialog mProgress;
		private Uri mUri;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mProgress = new ProgressDialog(activity);
			mProgress.setIndeterminate(true);
			mProgress.setCancelable(true);
			mProgress.setMessage("Loading image...");
			mProgress.setOnCancelListener(this);
			mProgress.show();
		}

		@Override
		protected Bitmap doInBackground(Uri... params) {
			mUri = params[0];
			Bitmap croppedBitmap = null;

			while (mImage.getWidth() < 1) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			try {
				Bitmap bitmap = decodeSampledBitmapFromFile(
						activity.getResources(), mUri, IMAGE_SIZE, IMAGE_SIZE);
				// Bitmap bitmap = MediaStore.Images.Media.getBitmap(
				// getContentResolver(), mUri);
				boolean landscape = bitmap.getWidth() > bitmap.getHeight();

				float scale_factor;
				if (landscape)
					scale_factor = (float) IMAGE_SIZE / bitmap.getHeight();
				else
					scale_factor = (float) IMAGE_SIZE / bitmap.getWidth();
				Matrix matrix = new Matrix();
				matrix.postScale(scale_factor, scale_factor);

				if (landscape) {
					int start = (bitmap.getWidth() - bitmap.getHeight()) / 2;
					croppedBitmap = Bitmap.createBitmap(bitmap, start, 0,
							bitmap.getHeight(), bitmap.getHeight(), matrix,
							true);
				} else {
					int start = (bitmap.getHeight() - bitmap.getWidth()) / 2;
					croppedBitmap = Bitmap.createBitmap(bitmap, 0, start,
							bitmap.getWidth(), bitmap.getWidth(), matrix, true);
				}
				// replace file
				OutputStream fOut = null;
				// File file = new File(getPath(mUri));
				try {
					fOut = new FileOutputStream(fileTemp);
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
				croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
				mUri = Uri.fromFile(fileTemp);

				// ---------------Used Media Scanner-----------
				// sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
				// Uri.parse("file://"
				// + Environment.getExternalStorageDirectory())));
			} catch (Exception e) {
			}

			return croppedBitmap;
		}

		public Bitmap decodeSampledBitmapFromFile(Resources res, Uri uri,
				int reqWidth, int reqHeight) {

			try {
				// First decode with inJustDecodeBounds=true to check dimensions
				final BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				AssetFileDescriptor fileDescriptor = null;
				fileDescriptor = activity.getContentResolver()
						.openAssetFileDescriptor(uri, "r");

				BitmapFactory.decodeFileDescriptor(
						fileDescriptor.getFileDescriptor(), null, options);

				// Calculate inSampleSize
				options.inSampleSize = calculateInSampleSize(options, reqWidth,
						reqHeight);

				// Decode bitmap with inSampleSize set
				options.inJustDecodeBounds = false;
				return BitmapFactory.decodeFileDescriptor(
						fileDescriptor.getFileDescriptor(), null, options);
			} catch (FileNotFoundException e) {
				return null;
			}
		}

		public int calculateInSampleSize(BitmapFactory.Options options,
				int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {

				// Calculate ratios of height and width to requested height and
				// width
				final int heightRatio = Math.round((float) height
						/ (float) reqHeight);
				final int widthRatio = Math.round((float) width
						/ (float) reqWidth);

				// Choose the smallest ratio as inSampleSize value, this will
				// guarantee
				// a final image with both dimensions larger than or equal to
				// the
				// requested height and width.
				inSampleSize = heightRatio < widthRatio ? heightRatio
						: widthRatio;
			}

			return inSampleSize;
		}

		@SuppressWarnings("deprecation")
		public String getPath(Uri uri) {
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = activity.managedQuery(uri, projection, null, null,
					null);
			activity.startManagingCursor(cursor);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);

			if (mProgress.getWindow() != null) {
				mProgress.dismiss();
			}

			if (result != null) {
				mBitmap = result;
				setImageURI(mUri, result);

				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				builder.setTitle("Filter Effect").setItems(
						new String[] { "Use Aviary", "No effect" },
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								switch (which) {
								case 0:
									if (isAviaryInstalled()) {
										openAviary();
									} else {
										openAviaryStore();
									}
									break;
								case 1:
									dialog.cancel();
									break;

								default:
									break;
								}
								// The 'which' argument contains the index
								// position
								// of the selected item
							}
						});
				builder.create();
				builder.show();
				// if (isWaitingAviary) {
				// startFeather(mUri);
				// isWaitingAviary = false;
				// }
			} else {
				Toast.makeText(activity, "Failed to load image " + mUri,
						Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onCancel(DialogInterface dialog) {
			this.cancel(true);
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}

	}

	private boolean setImageURI(final Uri uri, final Bitmap bitmap) {
		mImage.setImageBitmap(bitmap);
		mImage.setBackgroundDrawable(null);

		mImageUri = uri;
		return true;
	}

	private void openAviaryStore() {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri
				.parse("market://details?id=com.aviary.android.feather"));
		activity.startActivity(intent);
	}

	private void openAviary() {
		Intent newIntent = new Intent("aviary.intent.action.EDIT");
		newIntent.setDataAndType(getImageUri(), "image/*");
		newIntent.putExtra("app-id", activity.getPackageName());
	}

	private boolean isAviaryInstalled() {
		Intent intent = new Intent("aviary.intent.action.EDIT");
		intent.setType("image/*");
		List<ResolveInfo> list = activity.getPackageManager()
				.queryIntentActivities(intent,
						PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

}
