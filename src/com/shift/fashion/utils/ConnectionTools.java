package com.shift.fashion.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.text.format.Time;

public class ConnectionTools {
	public static String sendMultiPartForm(Context context, String url,
			ArrayList<NameValuePair> param, ArrayList<MultiPartPart> dataPart)
			throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		MultipartEntity reqEntity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);
		for (int a = 0, b = dataPart.size(); a < b; a++) {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			dataPart.get(a).getBitmap().compress(CompressFormat.PNG, 75, bos);
			byte[] data = bos.toByteArray();
			final Time time = new Time();
			time.setToNow();
			ByteArrayBody bab = new ByteArrayBody(data, Utils.md5(dataPart.get(
					a).getFileName()
					+ time.minute
					+ time.hour
					+ time.month
					+ time.monthDay
					+ time.year));
			reqEntity.addPart(dataPart.get(a).getParamName(), bab);
		}

		for (int a = 0, b = param.size(); a < b; a++) {
			reqEntity.addPart(param.get(a).getName(),
					new StringBody(param.get(a).getValue() + ""));
		}
		httppost.setEntity(reqEntity);

		HttpResponse response = httpClient.execute(httppost);
		HttpEntity entity = response.getEntity();
		String jsonText = EntityUtils.toString(entity);
		return jsonText;
	}

	public static String sendHttpGet(String address) throws IOException {
		String response = null;
		HttpClient httpclient = null;
		try {
			HttpGet httpget = new HttpGet(address);
			httpclient = new DefaultHttpClient();
			HttpResponse httpResponse = httpclient.execute(httpget);

			final int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				throw new Exception("Got HTTP " + statusCode + " ("
						+ httpResponse.getStatusLine().getReasonPhrase() + ')');
			}

			response = EntityUtils.toString(httpResponse.getEntity(),
					HTTP.UTF_8);

		} catch (Exception e) {
			throw new IOException("Error connection");

		} finally {
			if (httpclient != null) {
				httpclient.getConnectionManager().shutdown();
				httpclient = null;
			}
		}
		return response;
	}

	public static String sendHTTPPOST(final String url,
			final ArrayList<NameValuePair> data) {
		final String res;
		try {
			// http post
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			httppost.setEntity(new UrlEncodedFormEntity(data, "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();

			res = EntityUtils.toString(entity);
			// return responseText;
		} catch (Exception e) {
			return null;
		}
		return res;
	}
}
