package com.shift.fashion.utils;

import com.shift.fashion.utils.MyLocation.LocationResult;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.widget.Toast;


public abstract class LocationResultListener extends LocationResult {

	public abstract void onGotLocation(Location location);

	@Override
	public void gotLocation(Location location) {
		SharedPreferences sharedPreferences = mContext.getSharedPreferences(
				Constants.Pref.PREF_LOC, Activity.MODE_PRIVATE);
		String myLatitude = sharedPreferences.getString(Constants.Pref.MY_LAT, null);
		String myLongitude = sharedPreferences.getString(Constants.Pref.MY_LONG, null);
		if (location == null) {
			
			if (myLatitude != null && myLongitude != null) {
				// ambil last location dari sharedpreferences
				Location lastLocation = new Location("defLocation");
				lastLocation.setLatitude(Double.parseDouble(myLatitude));
				lastLocation.setLongitude(Double.parseDouble(myLongitude));
				location = lastLocation;
				((Activity) mContext).runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(
								mContext,
								"We can't determine your location. Now we use your last location",
								Toast.LENGTH_LONG).show();
					}
				});
			} else {
				// biarkan null
			}
		} else {
			myLatitude = location.getLatitude() + "";
			myLongitude = location.getLongitude() + "";
			Editor prefsPrivateEditor = sharedPreferences.edit();

			prefsPrivateEditor.putString(Constants.Pref.MY_LAT,
					location.getLatitude() + "");
			prefsPrivateEditor.putString(Constants.Pref.MY_LONG,
					location.getLongitude() + "");
			prefsPrivateEditor.putString(Constants.Pref.MY_ALT,
					location.getAltitude() + "");
			prefsPrivateEditor.commit();
		}
		onGotLocation(location);
	}
}