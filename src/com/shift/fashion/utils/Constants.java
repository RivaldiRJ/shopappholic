package com.shift.fashion.utils;

public class Constants {
	public static class APIUrl {
		public static String APP_PATH = "http://api.shopappholic.com-shift.tk/";
		public static String IMAGE_LINK = APP_PATH + "images/";
		public static String FEATURED = APP_PATH + "factory/featured";
		public static String LOGIN = APP_PATH + "user/login";
		public static String REGISTER = APP_PATH + "user/register";
		public static String LOGOUT = APP_PATH + "user/logout";
		public static String MY_PROFILE = APP_PATH + "user/my_profile";
		public static String FEATURED_CATEGORY = APP_PATH
				+ "factory/featured_category";
		public static String NEAR = APP_PATH + "factory/near";
		public static String CATEGORY = APP_PATH + "factory/category";
		public static String SEARCH = APP_PATH + "factory/search";
		public static String DETAIL = APP_PATH + "factory/detail";
		public static String FACTORY_REVIEW = APP_PATH + "factory/reviews";
		public static String FACTORY_UPDATE = APP_PATH + "factory/update";
		public static String REVIEW_ADD = APP_PATH + "review/add";
		public static String REVIEW_DELETE = APP_PATH + "review/del";
		public static String REVIEW_FACTORY = APP_PATH + "review/factory";
		public static String REVIEW_USER = APP_PATH + "review/user";
		public static String REVIEW_PARENT = APP_PATH + "review/parent";
	}

	public static class Pref {
		public final static String PREF_LOC = "pref_loc";
		public final static String SHOP_OBJECT = "SHOP_OBJECT";
		public final static String USER_DATA = "USER_DATA";

		public final static String MY_LAT = "my_lat";
		public final static String MY_LONG = "my_long";
		public final static String MY_ALT = "my_alt";

	}
}
