package com.shift.fashion.utils;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.myjson.Gson;
import com.shift.fashion.dao.UserDao;
import com.shift.fashion.utils.MyLocation.LocationResult;

public class Utils {
	public static String getMyLongitude(Context ctx) {
		SharedPreferences pref = ctx.getSharedPreferences(
				Constants.Pref.PREF_LOC, Activity.MODE_PRIVATE);
		return pref.getString(Constants.Pref.MY_LONG, null);
	}

	public static void SaveDataLogin(Context context, UserDao data) {
		final Gson gson = new Gson();
		CacheManager.writeAllCachedText(context, Constants.Pref.USER_DATA,
				gson.toJson(data));
	}
	public static void ShowDialog(Context context, String data) {
		ProgressDialog dialog = new ProgressDialog(context, ProgressDialog.THEME_TRADITIONAL);
		dialog.setMessage(data);
		dialog.show();
	}

	public static void LogOut(Context context) {
		CacheManager.writeAllCachedText(context, Constants.Pref.USER_DATA, "");
	}

	public static UserDao GetSavedLoginData(Context context) throws Exception {
		final Gson gson = new Gson();
		// CacheManager.writeAllCachedText(context, Constants.Pref.USER_DATA,
		// gson.toJson(data));
		String jsonText = CacheManager.readAllCachedText(context,
				Constants.Pref.USER_DATA);
		if (jsonText == null || jsonText.equals("")) {
			throw new Exception();
		}
		return gson.fromJson(jsonText, UserDao.class);
	}

	public static String getMyLatitude(Context ctx) {
		SharedPreferences pref = ctx.getSharedPreferences(
				Constants.Pref.PREF_LOC, Activity.MODE_PRIVATE);
		return pref.getString(Constants.Pref.MY_LAT, null);
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static String getMyAltitude(Context ctx) {
		SharedPreferences pref = ctx.getSharedPreferences(
				Constants.Pref.PREF_LOC, Activity.MODE_PRIVATE);
		return pref.getString(Constants.Pref.MY_ALT, null);
	}

	public static String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String h = Integer.toHexString(0xFF & messageDigest[i]);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String formatDistance(Float f) {
		return (Float.parseFloat(Math.round(f * 10) + "") / 10) + "";
	}

	public final static int RC_SETTING_LOCATION = 99;

	static MyLocation myLocation = new MyLocation();

	public static void startGetLocation(final Context ctx,
			LocationResult locationResult) {
		LocationResult mLocationResult = locationResult;
		locationResult.setContext(ctx);
		// GET LOCATION
		if (!myLocation.getLocation(ctx, mLocationResult)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
			builder.setMessage(
					"My location required to use this application. Do you want to enable it?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									Toast.makeText(
											ctx,
											"You must enable my location setting",
											Toast.LENGTH_SHORT).show();
									((Activity) ctx)
											.startActivityForResult(
													new Intent(
															android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
													RC_SETTING_LOCATION);
								}
							})
					.setNegativeButton("Exit",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
									((Activity) ctx).finish();
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		}
	}

}
