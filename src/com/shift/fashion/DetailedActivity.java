package com.shift.fashion;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.devsmart.android.ui.HorizontalListView;
import com.google.myjson.Gson;
import com.shift.fashion.adapter.HorizontalListViewAdapter;
import com.shift.fashion.api.DetailedFactoryApi;
import com.shift.fashion.api.DetailedFactoryApi.ApiDao;
import com.shift.fashion.api.DetailedFactoryApi.ApiResultListener;
import com.shift.fashion.dao.FactoryDao;
import com.shift.fashion.dao.ReviewDao;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.Utils;

public class DetailedActivity extends SherlockActivity {
	private FactoryDao factoryDao = null;
	AQuery aq;

	public static void startThisActivity(FactoryDao data, Activity ctx) {
		Intent intent = new Intent(ctx, DetailedActivity.class);
		final Gson gson = new Gson();
		intent.putExtra(Constants.Pref.SHOP_OBJECT,
				gson.toJson(data, FactoryDao.class));
		ctx.startActivity(intent);
	}

	DetailedFactoryApi api;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		menu.add(0, 1, 0, "Add Review")
				.setIcon(R.drawable.write_review)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		menu.add(0, 2, 2, "Refresh")
				.setIcon(R.drawable.ic_action_refresh)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		menu.add(0, 3, 1, "Direction")
				.setIcon(R.drawable.ic_action_directions)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			// menu.toggle();
			finish();
			break;
		case 1:
			try {
				Utils.GetSavedLoginData(this);
				ReviewActivity.startThisActivity(this, factoryDao);

			} catch (Exception e) {

				Toast.makeText(this, "Please login First", Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case 2:
			api.callApi(factoryDao.getId_toko());
			break;
		case 3:
			final Intent intent = new Intent(
					android.content.Intent.ACTION_VIEW,
					Uri.parse("http://maps.google.com/maps?saddr="
							+ Utils.getMyLatitude(this) + ","
							+ Utils.getMyLongitude(this) + "&daddr="
							+ factoryDao.getLat() + "," + factoryDao.getLng()
							+ ""));
			intent.setClassName("com.google.android.apps.maps",
					"com.google.android.maps.MapsActivity");
			startActivity(intent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.details_hop);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle("Detail");
		if (factoryDao == null) {
			initData();
		}else{
			getActionBar().setTitle("Test");	
		}
	}

	private void initData() {
		String JsonShopObject = getIntent().getStringExtra(
				Constants.Pref.SHOP_OBJECT);
		final Gson gson = new Gson();
		aq = new AQuery(this);
		factoryDao = gson.fromJson(JsonShopObject, FactoryDao.class);
		final TextView textJudul = (TextView) findViewById(R.id.textViewTitle);
		final TextView txtTitleProduct = (TextView) findViewById(R.id.txtTitleProduct);
		final TextView textDistance = (TextView) findViewById(R.id.textViewDistance);
		final TextView textAlamat = (TextView) findViewById(R.id.textViewAlamat);
		final RatingBar ratingBarDetail = (RatingBar) findViewById(R.id.ratingBarDetail);
		aq.id(R.id.imageDetail).image(factoryDao.getImage());
		// final ListView listReview = (ListView) findViewById(R.id.listReview);
		final LinearLayout layoutReview = (LinearLayout) findViewById(R.id.layout_review);
		final HorizontalListView lvProduct = (HorizontalListView) findViewById(R.id.listProduct);
		lvProduct.setVisibility(View.GONE);
		txtTitleProduct.setVisibility(View.GONE);
		textJudul.setText(factoryDao.getNama());

		textDistance.setText("Distance : "
				+ Utils.formatDistance(factoryDao.getDistance()));
		textAlamat.setText("Address : " + factoryDao.getAddress());
		api = new DetailedFactoryApi(this, new ApiResultListener() {

			@Override
			public void onApiResultError(String errorMessage) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onApiPreCall() {
				// TODO Auto-generated method stub
				layoutReview.removeAllViews();

			}

			@Override
			public void onApiResultOk(ApiDao data) {
				// TODO Auto-generated method stub
				layoutReview.removeAllViews();
				final List<ReviewDao> listData = data.getResult().getReviews();
				for (int a = 0, b = listData.size(); a < b; a++) {
					final View v = LayoutInflater.from(DetailedActivity.this)
							.inflate(R.layout.review_item, null);
					((TextView) v.findViewById(R.id.textViewUsername))
							.setText(listData.get(a).getUsername());
					((TextView) v.findViewById(R.id.textViewReview))
							.setText(listData.get(a).getComment());
					// if(a%2 == 1){
					// aq.id(R.id.imageViewReview).image(R.drawable.dummy_review_2);
					// }else{
					// aq.id(R.id.imageViewReview).image(R.drawable.dummy_review_image);
					// }
					final ImageView imgView = (ImageView) v
							.findViewById(R.id.imageViewReview);

					if (listData.get(a).getImage() != null
							&& listData.get(a).getImage().startsWith("http")) {
						aq.id(imgView).image(listData.get(a).getImage(), true,
								true);

					} else {
						imgView.setVisibility(View.GONE);
					}
					layoutReview.addView(v);
				}
				try {
					ratingBarDetail.setRating(Float.parseFloat(data.getResult()
							.getRating().getAverage()));
					ratingBarDetail.setVisibility(View.VISIBLE);
				} catch (Exception e) {

				}
				if (data.getResult().getProduk().size() > 0) {
					HorizontalListViewAdapter adapter = new HorizontalListViewAdapter(
							data.getResult().getProduk(), DetailedActivity.this);
					lvProduct.setAdapter(adapter);
					lvProduct.setVisibility(View.VISIBLE);
					txtTitleProduct.setVisibility(View.VISIBLE);
				} else {
					txtTitleProduct
							.setText("Currently have no product to show!\n ");
					txtTitleProduct.setVisibility(View.VISIBLE);

				}

			}
		});
		api.callApi(factoryDao.getId_toko());

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		api.cancelAPI();
		super.onBackPressed();
	}
}
