package com.shift.fashion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.shift.fashion.api.CategoryApi;
import com.shift.fashion.api.CategoryApi.ApiDao;
import com.shift.fashion.api.CategoryApi.ApiResultListener;

public class FragmentStore extends SherlockFragment implements
		OnPageChangeListener, OnTabChangeListener {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// Initialise the TabHost
		final View v = inflater.inflate(R.layout.fragment_store, container,
				false);
		this.initialiseTabHost(savedInstanceState, v);
		// if (savedInstanceState != null) {
		// mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); //
		// set
		// // the
		// // tab
		// // as
		// // per
		// // the
		// // saved
		// // state
		// }
		// Intialise ViewPager
		this.intialiseViewPager(v, savedInstanceState);

		// return super.onCreateView(inflater, container, savedInstanceState);
		return v;
	}

	public class PagerAdapter extends FragmentPagerAdapter {

		private List<Fragment> fragments;

		public void addPage(Fragment page) {
			fragments.add(page);
			this.getItem(getCount() - 1);
		}

		/**
		 * @param fm
		 * @param fragments
		 */
		public PagerAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);

			this.fragments = fragments;
		}

		public PagerAdapter(FragmentManager fm) {
			super(fm);
			// final List<Fragment> fragments =

			this.fragments = new Vector<Fragment>();
			;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
		 */
		@Override
		public Fragment getItem(int position) {
			return this.fragments.get(position);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v4.view.PagerAdapter#getCount()
		 */
		@Override
		public int getCount() {
			return this.fragments.size();
		}
	}

	// copas state mode
	private TabHost mTabHost;
	private ViewPager mViewPager;
	private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, FragmentStore.TabInfo>();
	private PagerAdapter mPagerAdapter;

	/**
	 * 
	 * @author mwho Maintains extrinsic info of a tab's construct
	 */
	private class TabInfo {
		private String tag;
		private Class<?> clss;
		private Bundle args;
		private Fragment fragment;

		TabInfo(String tag, Class<?> clazz, Bundle args) {
			this.tag = tag;
			this.clss = clazz;
			this.args = args;
		}

	}

	/**
	 * A simple factory that returns dummy views to the Tabhost
	 * 
	 * @author mwho
	 */
	class TabFactory implements TabContentFactory {

		private final Context mContext;

		/**
		 * @param context
		 */
		public TabFactory(Context context) {
			mContext = context;
		}

		/**
		 * (non-Javadoc)
		 * 
		 * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
		 */
		public View createTabContent(String tag) {
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	// protected void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// // Inflate the layout
	// setContentView(R.layout.tabs_viewpager_layout);
	// }

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onSaveInstanceState(android.os.Bundle)
	 */
	// protected void onSaveInstanceState(Bundle outState) {
	// // outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab
	// // selected
	// // super.onSaveInstanceState(outState);
	//
	// }
	private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		return view;
	}

	/**
	 * Initialise ViewPager
	 */
	private void intialiseViewPager(final View v, final Bundle args) {

		//
		this.mViewPager = (ViewPager) v.findViewById(R.id.pager_store_category);
		this.mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub
				FragmentStore.this.mTabHost.setCurrentTab(position);

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

		// TODO INI KERJAIN
		final CategoryApi api = new CategoryApi(getActivity(),
				new ApiResultListener() {

					@Override
					public void onApiResultError(String errorMessage) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), errorMessage,
								Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onApiPreCall() {
						// TODO Auto-generated method stub

					}

					@Override
					public void onApiResultOk(ApiDao data) {
						// TODO Auto-generated method stub
						List<Fragment> fragments = new ArrayList<Fragment>();

						for (int a = 0, b = data.getResult().size(); a < b; a++) {
							final ListStoreFragment fl = new ListStoreFragment();
							final String category = data.getResult().get(a)
									.getCategory();
							Toast.makeText(getActivity(), category,
									Toast.LENGTH_SHORT).show();
							TabInfo tabInfo = null;
							FragmentStore.AddTab(
									FragmentStore.this,
									FragmentStore.this.mTabHost,
									FragmentStore.this.mTabHost.newTabSpec(
											category).setIndicator(createTabView(getActivity(), category)).setContent(new TabContentFactory() {
												
												@Override
												public View createTabContent(String arg0) {
													// TODO Auto-generated method stub
													return new TextView(getActivity());
												}
											}),
									(tabInfo = new TabInfo(category,
											ListStoreFragment.class, args)),
									getActivity());

							// mPagerAdapter.addPage(fl);
							// mPagerAdapter.notifyDataSetChanged();
							final Bundle bundle = new Bundle();
							bundle.putString("category", category);
							fl.setArguments(bundle);
							fragments.add(fl);
							// fl.setCategory(category, new CustomListener() {
							//
							// @Override
							// public void onReponse(boolean isOkay) {
							// // TODO Auto-generated method stub
							// if (isOkay) {
							//
							// FragmentStore.this.mapTabInfo.put(
							// tabInfo.tag, tabInfo);
							// }
							// }
							// });
						}
						FragmentStore.this.mPagerAdapter = new PagerAdapter(
								getActivity().getSupportFragmentManager(),
								fragments);
						FragmentStore.this.mViewPager
								.setAdapter(FragmentStore.this.mPagerAdapter);
						// for (int a = 0, b = data.getResult().size(); a < b;
						// a++) {
						// final String category = data.getResult().get(a)
						// .getCategory();
						// ListStoreFragment fl = (ListStoreFragment)
						// FragmentStore.this.mPagerAdapter
						// .getItem(a);
						// fl.setCategory(category, getActivity());
						// }
						// FragmentStore.AddTab(this, this.mTabHost,
						// this.mTabHost.newTabSpec("Tab2").setIndicator("Tab 2"),
						// (tabInfo = new TabInfo("Tab2",
						// FragmentListStore.class, args)),
						// getActivity());

						// disini namain tab
						// fragments.add(Fragment.instantiate(getActivity(),
						// FragmentListStore.class.getName()));
						// fragments.add(Fragment.instantiate(getActivity(),
						// FragmentListStore.class.getName()));

					}
				});
		api.callApi();
		// fragments.add(new FragmentListStore());
		// fragments.add(Fragment.instantiate(getActivity(),
		// Tab2Fragment.class.getName()));
		// fragments.add(Fragment.instantiate(getActivity(),
		// Tab3Fragment.class.getName()));
	}

	/**
	 * Initialise the Tab Host
	 */
	private void initialiseTabHost(Bundle args, View v) {
		mTabHost = (TabHost) v.findViewById(android.R.id.tabhost);
		mTabHost.setup();
		// TabsViewPagerFragmentActivity.AddTab(this, this.mTabHost,
		// this.mTabHost
		// .newTabSpec("Tab2").setIndicator("Tab 2"),
		// (tabInfo = new TabInfo("Tab2", Tab2Fragment.class, args)));
		// this.mapTabInfo.put(tabInfo.tag, tabInfo);
		// TabsViewPagerFragmentActivity.AddTab(this, this.mTabHost,
		// this.mTabHost
		// .newTabSpec("Tab3").setIndicator("Tab 3"),
		// (tabInfo = new TabInfo("Tab3", Tab3Fragment.class, args)));
		// this.mapTabInfo.put(tabInfo.tag, tabInfo);
		// Default to first tab
		// this.onTabChanged("Tab1");
		//
		mTabHost.setOnTabChangedListener(this);
	}

	/**
	 * Add Tab content to the Tabhost
	 * 
	 * @param parent
	 * @param tabHost
	 * @param tabSpec
	 * @param clss
	 * @param args
	 */
	private static void AddTab(FragmentStore parent, TabHost tabHost,
			TabHost.TabSpec tabSpec, TabInfo tabInfo, Context ctx) {
		// Attach a Tab view factory to the spec
		// ;

		tabSpec.setContent(parent.new TabFactory(ctx));

		tabHost.addTab(tabSpec);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.widget.TabHost.OnTabChangeListener#onTabChanged(java.lang.String)
	 */
	public void onTabChanged(String tag) {
		// TabInfo newTab = this.mapTabInfo.get(tag);
		int pos = this.mTabHost.getCurrentTab();
//		Toast.makeText(getActivity(), "ini pas tab changed " + pos,
//				Toast.LENGTH_SHORT).show();
		this.mViewPager.setCurrentItem(pos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrolled
	 * (int, float, int)
	 */
	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.view.ViewPager.OnPageChangeListener#onPageSelected
	 * (int)
	 */
	@Override
	public void onPageSelected(int position) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), "ini pas page changed" + position,
				Toast.LENGTH_SHORT).show();
		this.mTabHost.setCurrentTab(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.view.ViewPager.OnPageChangeListener#
	 * onPageScrollStateChanged(int)
	 */
	@Override
	public void onPageScrollStateChanged(int state) {
		// TODO Auto-generated method stub

	}
}
