package com.shift.fashion;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.shift.fashion.utils.LocationResultListener;
import com.shift.fashion.utils.Utils;

public class SplashActivity extends Activity {
	private Context ctx = this;

	private Thread splashTread;

	protected int splashTime = 3000; // time to display the splash screen in ms
	private boolean splashActive = true;
	
	LocationResultListener mLocationResult;

	private TextView progressSplash;

	Location mLocation = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//TODO kerjakeun di acaraka
		setContentView(R.layout.splash);
//
//		progressSplash = (TextView) findViewById(R.id.progress_splash);

//		if (Utils.isLoggedIn(this)) {
//			myProfileApi = new MyProfileApi(this, myProfileApiListener);
//			myProfileApi.callApi();
//		}

		startSplash();
	}

//	MyProfileApi.ApiResultListener myProfileApiListener = new MyProfileApi.ApiResultListener() {
//
//		@Override
//		public void onApiResultError(String errorMessage) {
//		}
//
//		@Override
//		public void onApiPreCall() {
//		}
//
//		@Override
//		public void onApiResultOk(UserDao userDao) {
//
//		}
//	};

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean("DUMMY", true);
	}

	private void startSplash() {
		// start splash screen
		// thread for displaying the SplashScreen
		splashTread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while (splashActive || waited < splashTime) {
						sleep(100);
						waited += 100;
					}
					// hilangkan splash screen dan bikin ga fullscreen
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							removeSplash();
						}
					});
				} catch (InterruptedException e) {
					// do nothing
				}
			}
		};
		splashTread.start();
//		progressSplash.setText("Finding your location....");
		mLocationResult = locationResultListener;
		Utils.startGetLocation(this, mLocationResult);
	}

	LocationResultListener locationResultListener = new LocationResultListener() {

		@Override
		public void onGotLocation(Location location) {
			mLocation = location;
			((Activity) ctx).runOnUiThread(new Runnable() {
				@Override
				public void run() {
//					progressSplash.setText("Loading...");
				}
			});

			splashActive = false;
		}
	};

	private void removeSplash() {
		finish();
		Intent intent;
		if (mLocation == null) {
//			intent = new Intent(ctx, SearchLocationActivity.class);
//			startActivity(intent);
		} else {
			intent = new Intent(ctx, ShopActivity.class);
			startActivity(intent);
			//			 .startThisActivity(this);
		}
	}

	@Override
	protected void onDestroy() {
		if (splashTread != null)
			splashTread.interrupt();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case Utils.RC_SETTING_LOCATION:
			Utils.startGetLocation(this, mLocationResult);
			break;
		default:
			break;
		}
	}
}