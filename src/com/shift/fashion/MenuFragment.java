package com.shift.fashion;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class MenuFragment extends SherlockFragment implements
		OnItemClickListener {

	private ShopActivity SA;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View v = inflater.inflate(R.layout.fragment_menu, null);
		// v.findViewById(R.id.btnAbout).setOnClickListener(this);
		SA = ((ShopActivity) getSherlockActivity());
		final ListView lvNavigator = (ListView) v
				.findViewById(R.id.listViewNavigator);
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_list_item_1, SA.StringId);
		lvNavigator.setAdapter(adapter);
		lvNavigator.setOnItemClickListener(this);

		// v.findViewById(R.id.btnFeedback).setOnClickListener(this);
		return v;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		final SlidingMenu menu = SA.menu;
		SA.OnTabChangedFromSlider(SA.StringId.get(position));
		menu.toggle();
	}

	// @Override
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// final Intent intent;
	// switch (v.getId()) {
	// case R.id.btnAbout:
	// // intent = new Intent(getActivity(), AboutActivity.class);
	// // startActivity(intent);
	// break;
	// case R.id.btnFeedback:
	// // intent = new Intent(getActivity(), FeedbackActivity.class);
	// // startActivity(intent);
	// break;
	//
	// default:
	// break;
	// }
	// }
}
