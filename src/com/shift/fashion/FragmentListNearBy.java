package com.shift.fashion;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.shift.fashion.adapter.ShopListAdapter;
import com.shift.fashion.api.NearByApi;
import com.shift.fashion.api.NearByApi.ApiDao;
import com.shift.fashion.api.NearByApi.ApiResultListener;

public class FragmentListNearBy extends SherlockFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View v = inflater.inflate(R.layout.list_store, container, false);
		final PullToRefreshListView lvStrore = ((PullToRefreshListView) v
				.findViewById(R.id.listViewStore));

		final NearByApi api = new NearByApi(getActivity(),
				new ApiResultListener() {

					@Override
					public void onApiResultError(String errorMessage) {
						// TODO Auto-generated method stub
						lvStrore.onRefreshComplete();
						Toast.makeText(getActivity(), errorMessage,
								Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onApiPreCall() {
						// TODO Auto-generated method stub

					}

					@Override
					public void onApiResultOk(final ApiDao data) {
						// TODO Auto-generated method stub
						lvStrore.onRefreshComplete();
						final ShopListAdapter adapter = new ShopListAdapter(
								getActivity(), data.getResult());
						// final ArrayAdapter<String> adapter = new
						// ArrayAdapter<String>(
						// getActivity(), android.R.layout.simple_list_item_1);
						// adapter.add("Store 1 ");
						// adapter.add("Store 1 ");
						// adapter.add("Store 1 ");
						// adapter.add("Store 1 ");
						// adapter.add("Store 1 ");
						// adapter.add("Store 1 ");

						lvStrore.setAdapter(adapter);
						lvStrore.setVisibility(View.VISIBLE);
						v.findViewById(R.id.progressBarStore).setVisibility(
								View.GONE);
						lvStrore.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int position, long arg3) {
								// TODO Auto-generated method stub
								DetailedActivity.startThisActivity(data
										.getResult().get(position-1),
										getActivity());

							}

						});
					}
				});
		api.callApi();
		lvStrore.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				api.callApi();
			}
		});

		// return super.onCreateView(inflater, container, savedInstanceState);
		return v;
	}

}
