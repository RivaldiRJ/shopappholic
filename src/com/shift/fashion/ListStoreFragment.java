package com.shift.fashion;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.shift.fashion.adapter.ShopListAdapter;
import com.shift.fashion.api.CategoryListApi;
import com.shift.fashion.api.CategoryListApi.ApiDao;
import com.shift.fashion.api.CategoryListApi.ApiResultListener;

public class ListStoreFragment extends SherlockFragment {
	private View v;
	// private LayoutInflater inflater;
	// private ViewGroup container;
	// private FragmentListStore fragmentListStore;
	private PullToRefreshListView lvStore;

	// public ListStoreFragment(String as) {
	// // TODO Auto-generated constructor stub
	//
	//
	// }

	// public void setCategory(final String Category, final Context ctx) {

	//
	// // final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
	// // getActivity(), android.R.layout.simple_list_item_1);
	// // adapter.add("Store 1 ");
	// // adapter.add("Store 1 ");
	// // adapter.add("Store 1 ");
	// // adapter.add("Store 1 ");
	// // adapter.add("Store 1 ");
	// // adapter.add("Store 1 ");
	// // fragmentListStore.callData(Category);
	//
	// }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// this.container = container;
		// this.inflater = inflater;
		// Toast.makeText(getActivity(), text, duration)
		v = inflater.inflate(R.layout.list_store, container, false);
		lvStore = (PullToRefreshListView) v.findViewById(R.id.listViewStore);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);
		adapter.add("Getting Data......");
		lvStore = (PullToRefreshListView) v.findViewById(R.id.listViewStore);
		lvStore.setAdapter(adapter);
		// setCategory(, getActivity());

		final CategoryListApi api = new CategoryListApi(getActivity(),
				new ApiResultListener() {

					@Override
					public void onApiResultError(String errorMessage) {
						// TODO Auto-generated method stub
						// Toast.makeText(getSherlockActivity(), errorMessage,
						// Toast.LENGTH_SHORT).show();
						lvStore.onRefreshComplete();
						Toast.makeText(
								getActivity(),
								"Failed to get data "
										+ getArguments().getString("category")
										+ "\n" + "Reason : " + errorMessage,
								Toast.LENGTH_SHORT).show();

						// srl
						// listener.onReponse(false);

					}

					@Override
					public void onApiPreCall() {
						// TODO Auto-generated method stub

					}

					@Override
					public void onApiResultOk(final ApiDao data) {
						// TODO Auto-generated method stub
						lvStore.onRefreshComplete();

						if (data.getResult().getShopappholic().size() > 0) {
							// listener.onReponse(true);
						}
						// Toast.makeText(getActivity(), Category,
						// Toast.LENGTH_SHORT).show();
						final ShopListAdapter adapter = new ShopListAdapter(
								getActivity(), data.getResult()
										.getShopappholic());
						final ListView lvStore = (ListView) v
								.findViewById(R.id.listViewStore);
						lvStore.setAdapter(adapter);
						// lvStore.setListAdapter(adapter);
						lvStore.setVisibility(View.VISIBLE);
						v.findViewById(R.id.progressBarStore).setVisibility(
								View.GONE);

						lvStore.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> adapter,
									View v, int position, long id) {
								// TODO bikin buat detail di
								// sini
								DetailedActivity.startThisActivity(
										data.getResult().getShopappholic()
												.get(position - 1),
										getActivity());
								// data.getResult().getShopappholic().get(position)
							}
						});
					}
				});
		api.callApi(getArguments().getString("category"));
		lvStore.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				api.callApi(getArguments().getString("category"));

			}
		});
		// return super.onCreateView(inflater, container, savedInstanceState);
		// fragmentListStore = (FragmentListStore)
		// getFragmentManager().findFragmentById(R.id.listViewStore);
		return v;
	}
}
