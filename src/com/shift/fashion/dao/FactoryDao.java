package com.shift.fashion.dao;

public class FactoryDao {
	String id_toko, nama, is_active, address, contact, link, about, image,
			info, lat, lng, currency, minimal, maximal, facility, keywords,
			status, lastvisit, user_added, add_time, user_modified,
			modify_time, id_category, distance, produk_image, review_count;


	

	public String getAbout() {
		return about;
	}

	public String getAdd_time() {
		return add_time;
	}

	public String getAddress() {
		return address;
	}

	public String getContact() {
		return contact;
	}

	public String getCurrency() {
		return currency;
	}

	public float getDistance() {
		return Float.parseFloat(distance);
	}

	public String getFacility() {
		return facility;
	}

	public String getId_category() {
		return id_category;
	}

	public String getId_toko() {
		return id_toko;
	}

	public String getImage() {
		return image;
	}

	public String getInfo() {
		return info;
	}

	public String getIs_active() {
		return is_active;
	}

	public String getKeywords() {
		return keywords;
	}

	public String getLastvisit() {
		return lastvisit;
	}

	public String getLat() {
		return lat;
	}

	public String getLink() {
		return link;
	}

	public String getLng() {
		return lng;
	}

	public String getMaximal() {
		return maximal;
	}

	public String getMinimal() {
		return minimal;
	}

	public String getModify_time() {
		return modify_time;
	}

	public String getNama() {
		return nama;
	}

	public String getProduk_image() {
		return produk_image;
	}

	// public String getRating() {
	// return rating;
	// }

	public String getReview_count() {
		return review_count;
	}

	public String getStatus() {
		return status;
	}

	public String getUser_added() {
		return user_added;
	}

	public String getUser_modified() {
		return user_modified;
	}
}
