package com.shift.fashion.dao;

import java.util.List;

public class DetailedFactoryDao extends FactoryDao {
	public Rating getRating() {
		return rating;
	}

	Rating rating;
	List<ProdukDao> produk;

	public List<ProdukDao> getProduk() {
		return produk;
	}

	public class Rating {
		String average, one, two, three, four, five;

		public String getAverage() {
			return average;
		}

		public String getFive() {
			return five;
		}

		public String getFour() {
			return four;
		}

		public String getOne() {
			return one;
		}

		public String getThree() {
			return three;
		}

		public String getTwo() {
			return two;
		}
	}

	List<ReviewDao> reviews;

	public List<ReviewDao> getReviews() {
		return reviews;
	}
}
