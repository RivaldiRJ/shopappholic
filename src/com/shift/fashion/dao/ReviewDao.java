package com.shift.fashion.dao;

public class ReviewDao {
	String review, id_user, id_toko, parent, comment, image, rating,
			is_checkin, add_time, count_comment, id_review, username;
	FactoryDao tbl_toko;

	public String getId_review() {
		return id_review;
	}

	public String getUsername() {
		return username;
	}

	public FactoryDao getTbl_toko() {
		return tbl_toko;
	}

	public String getAdd_time() {
		return add_time;
	}

	public String getComment() {
		return comment;
	}

	public String getCount_comment() {
		return count_comment;
	}

	public String getId_toko() {
		return id_toko;
	}

	public String getId_user() {
		return id_user;
	}

	public String getImage() {
		return image;
	}

	public String getIs_checkin() {
		return is_checkin;
	}

	public String getParent() {
		return parent;
	}

	public String getRating() {
		return rating;
	}

	public String getReview() {
		return review;
	}
}
