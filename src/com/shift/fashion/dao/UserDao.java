package com.shift.fashion.dao;

public class UserDao extends BaseApiDao {
	String id_user, username, email, nama, sex, phone, tgl_lahir, url, about,
			facebook, twitter, registered, lastvisit, token, image;

	public String getImage() {
		return image;
	}

	public String getAbout() {
		return about;
	}

	public String getEmail() {
		return email;
	}

	public String getFacebook() {
		return facebook;
	}

	public String getId_user() {
		return id_user;
	}

	public String getLastvisit() {
		return lastvisit;
	}

	public String getNama() {
		return nama;
	}

	public String getPhone() {
		return phone;
	}

	public String getRegistered() {
		return registered;
	}

	public String getSex() {
		return sex;
	}

	public String getTgl_lahir() {
		return tgl_lahir;
	}

	public String getToken() {
		return token;
	}

	public String getTwitter() {
		return twitter;
	}

	public String getUrl() {
		return url;
	}

	public String getUsername() {
		return username;
	}
}
