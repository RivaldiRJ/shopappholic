package com.shift.fashion.dao;

public class ProdukDao {
	String id_produk, is_active, produk, image, description, resized, id_toko,
			harga, tag, status, user_added, lastvisit, add_time, user_modified,
			modify_time;

	public String getAdd_time() {
		return add_time;
	}

	public String getDescription() {
		return description;
	}

	public String getHarga() {
		return harga;
	}

	public String getId_produk() {
		return id_produk;
	}

	public String getId_toko() {
		return id_toko;
	}

	public String getImage() {
		return image;
	}

	public String getIs_active() {
		return is_active;
	}

	public String getLastvisit() {
		return lastvisit;
	}

	public String getModify_time() {
		return modify_time;
	}

	public String getProduk() {
		return produk;
	}

	public String getResized() {
		return resized;
	}

	public String getStatus() {
		return status;
	}

	public String getTag() {
		return tag;
	}

	public String getUser_added() {
		return user_added;
	}

	public String getUser_modified() {
		return user_modified;
	}
}
