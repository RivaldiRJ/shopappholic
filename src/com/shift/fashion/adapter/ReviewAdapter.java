package com.shift.fashion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.shift.fashion.R;
import com.shift.fashion.dao.ReviewDao;

public class ReviewAdapter extends BaseAdapter {
	final ArrayList<ReviewDao> data = new ArrayList<ReviewDao>();
	final Activity activity;

	public ReviewAdapter(final Activity activity, final List<ReviewDao> data) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.data.addAll(data);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View v, ViewGroup container) {
		// TODO Auto-generated method stub
		if (v == null) {
			final LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.review_item, null);
		}
		((TextView) v.findViewById(R.id.textViewUsername)).setText(data.get(
				position).getUsername());
		((TextView) v.findViewById(R.id.textViewReview)).setText(data.get(
				position).getComment());
		if (data.get(position).getImage() != null
				&& data.get(position).getImage().startsWith("http")) {
			AQuery aq = new AQuery(activity);
			aq.id(R.id.imageViewReview).image(data.get(position).getImage());
		} else {
			v.findViewById(R.id.imageViewReview).setVisibility(View.GONE);
		}
		return v;
	}

}
