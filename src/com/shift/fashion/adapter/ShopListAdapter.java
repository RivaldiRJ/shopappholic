package com.shift.fashion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.shift.fashion.R;
import com.shift.fashion.dao.FactoryDao;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.Utils;

public class ShopListAdapter extends BaseAdapter {
	final Context ctx;
	final ArrayList<FactoryDao> data = new ArrayList<FactoryDao>();

	public ShopListAdapter(final Context ctx, final List<FactoryDao> list) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.data.addAll(list);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View v, ViewGroup arg2) {
		// TODO Auto-generated method stub
		// Toast.makeText(ctx, arg0+" " +data.get(arg0).getNama() ,
		// Toast.LENGTH_SHORT).show();
		final FactoryDao xx = data.get(arg0);

		if (v == null) {
			final LayoutInflater inflater = (LayoutInflater) ctx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.row_item_shop, null);
		}
		((TextView) v.findViewById(R.id.tv_address)).setText(data.get(arg0)
				.getAddress());
		((TextView) v.findViewById(R.id.tv_title)).setText(xx.getNama());
		try {
			((TextView) v.findViewById(R.id.tv_distance)).setText(Utils
					.formatDistance(data.get(arg0).getDistance()) + " Km");
		} catch (Exception e) {

		}

		final AQuery aq = new AQuery(ctx);
		final String LinkImage = data.get(arg0).getImage();
		aq.id(R.id.imageShop).image(LinkImage);

		return v;
	}

}
