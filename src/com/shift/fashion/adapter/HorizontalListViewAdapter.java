package com.shift.fashion.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.shift.fashion.R;
import com.shift.fashion.dao.ProdukDao;

public class HorizontalListViewAdapter extends BaseAdapter {
	final ArrayList<ProdukDao> data = new ArrayList<ProdukDao>();
//	final String Kota;
	final Activity activity;

	public HorizontalListViewAdapter(final List<ProdukDao> data,
			final Activity activity) {
		// TODO Auto-generated constructor stub
		this.data.addAll(data);
		this.activity = activity;
//		this.Kota = Kota;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position + 1;
	}

	@Override
	public View getView(final int position, View v, final ViewGroup parent) {

		// if (position == data.size() - 1) {
		// // Toast.makeText(activity, "asdasdasdasd",
		// // Toast.LENGTH_LONG).show();
		// new GetData().execute();
		// }
		// TODO Auto-generated method stub
		if (v == null) {
			final LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.horizontal_list_item, null);
		}
		final ProdukDao item = data.get(position);
		final TextView txtHospName = (TextView) v
				.findViewById(R.id.hospital_name);
		final TextView txtHospDistance = (TextView) v
				.findViewById(R.id.hospital_distance);
		final ImageView imgView = (ImageView) v
				.findViewById(R.id.hospital_image);
		if (item.getImage() == null || item.getImage().equals("")
				|| item.getImage().equals("null")) {
			// imgView.setBackgroundColor(android.R.color.holo_blue_dark);
			imgView.setVisibility(View.GONE);
		} else {
			AQuery aq = new AQuery(activity);
			aq.id(R.id.hospital_image).image(item.getImage(),true,true);
			// ImageLoadingListener animateFirstListener = new
			// AnimateFirstDisplayListener();

			// imageLoader.displayImage(
			// Utils.getImageRestoPath(resto.getId(), resto.getImage()),
			// ivLogo, animateFirstListener);
//			if (this.Kota.equals("Lampung")) {
				// ImageLoader.getInstance().displayImage(Utils.getImageRestoPath(item.getId(),
				// item.getImage()), imgView,animateFirstListener);
//			} else {
//				// ImageLoader.getInstance().displayImage(Utils.getImageRestoPath(item.getId(),
//				// item.getImage()), imgView,animateFirstListener);
//			}

		}
		txtHospName.setText(item.getProduk());

//		txtHospDistance.setText(item.getDistance());
		txtHospDistance.setVisibility(View.GONE);
		return v;
	}

}
