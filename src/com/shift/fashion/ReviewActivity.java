package com.shift.fashion;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.myjson.Gson;
import com.shift.fashion.api.ReviewAddApi;
import com.shift.fashion.api.ReviewAddApi.ApiDao;
import com.shift.fashion.api.ReviewAddApi.ApiResultListener;
import com.shift.fashion.dao.FactoryDao;
import com.shift.fashion.dao.UserDao;
import com.shift.fashion.utils.AviaryHelper;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.ImageUtils;
import com.shift.fashion.utils.Utils;

import eu.janmuller.android.simplecropimage.CropImage;

public class ReviewActivity extends SherlockActivity {
	private static final int ACTION_REQUEST_CAMERA = 98;
	private static final int ACTION_REQUEST_GALLERY = 99;
	private static final int ACTION_REQUEST_FEATHER = 100;
	public static final int ACTION_REQUEST_FACTORY = 123;
	private static final int ACTION_REQUEST_CROP = 101;
	private static final int EXTERNAL_STORAGE_UNAVAILABLE = 1;
	private static final int CHOOSE_IMAGE_METHOD = 2;
	private AviaryHelper aviaryHelper;
	Uri imgUri;
	String imgPath;

	private FactoryDao factory = null;
	final Gson gson = new Gson();
	private TextView txtUsername;
	private RatingBar ratingBar;
	private ImageView imageReview;
	private EditText txtComment;
	private UserDao user;
	private Button buttonFactory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_review_layout);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		txtUsername = (TextView) findViewById(R.id.textViewUsername);
		ratingBar = (RatingBar) findViewById(R.id.ratingBarReview);
		imageReview = (ImageView) findViewById(R.id.imageViewAddReview);
		txtComment = (EditText) findViewById(R.id.editTextReview);
		aviaryHelper = new AviaryHelper(this, imageReview);
		buttonFactory = (Button) findViewById(R.id.buttonFactory);
		buttonFactory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SearchActivity.startThisActivity(ReviewActivity.this,
						ACTION_REQUEST_FACTORY);
			}
		});
		imageReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog(CHOOSE_IMAGE_METHOD);

			}
		});
		try {
			user = Utils.GetSavedLoginData(this);
			txtUsername.setText(user.getUsername());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			String jsonText = getIntent().getStringExtra(
					Constants.Pref.SHOP_OBJECT);
			factory = gson.fromJson(jsonText, FactoryDao.class);
			doFactory(factory);
		} catch (Exception e) {

		}
	}

	private void doFactory(FactoryDao data) {
		buttonFactory.setText("Selected : " + data.getNama());
	}

	public static void startThisActivity(Activity ctx) {
		Intent intent = new Intent(ctx, ReviewActivity.class);
		ctx.startActivityForResult(intent, 2);
	}

	public static void startThisActivity(Activity ctx, FactoryDao dao) {
		Intent intent = new Intent(ctx, ReviewActivity.class);
		final Gson gson = new Gson();
		intent.putExtra(Constants.Pref.SHOP_OBJECT, gson.toJson(dao));
		ctx.startActivityForResult(intent, 2);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			// menu.toggle();
			finish();
			break;
		case 1:
			// TODO buat send data
			final ProgressDialog dialog;
			dialog = new ProgressDialog(this);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setMessage("Adding Review....");
			dialog.setTitle("Shop App Holic");
			final ReviewAddApi api = new ReviewAddApi(this,
					new ApiResultListener() {

						@Override
						public void onApiResultError(String errorMessage) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							Toast.makeText(getApplicationContext(),
									errorMessage, Toast.LENGTH_SHORT).show();

						}

						@Override
						public void onApiPreCall() {
							// TODO Auto-generated method stub
							dialog.show();

						}

						@Override
						public void onApiResultOk(ApiDao data) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							Toast.makeText(getApplicationContext(),
									"Review added", Toast.LENGTH_SHORT).show();
							finish();
						}
					});

			if (factory == null) {
				Toast.makeText(this, "Please select a factory",
						Toast.LENGTH_SHORT).show();
				SearchActivity.startThisActivity(this, ACTION_REQUEST_FACTORY);
			} else {
				api.callAPI(factory.getId_toko(), txtComment.getText()
						.toString(), "0", user.getId_user(), user.getToken(),
						aviaryHelper.getBitmap(), ratingBar.getRating());

			}
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case ACTION_REQUEST_CAMERA:
				// String filepath = ImageUtils.getRealPath(this,
				// aviaryHelper.getImageUri());
				ImageUtils.startCropImage(this, aviaryHelper.getImageUri()
						.getPath(), ACTION_REQUEST_CROP);
				break;
			case ACTION_REQUEST_GALLERY:
				aviaryHelper.copyImageToTemp(this, data.getData());

				// filepath = ImageUtils.getRealPath(this,
				// aviaryHelper.getImageUri());
				ImageUtils.startCropImage(this, aviaryHelper.getImageUri()
						.getPath(), ACTION_REQUEST_CROP);
				break;
			case ACTION_REQUEST_CROP:
				File f = new File(data.getStringExtra(CropImage.IMAGE_PATH));
				imgUri = Uri.fromFile(f);
				imgPath = f.getPath();
				aviaryHelper.loadAsync(imgUri);
				break;

			case ACTION_REQUEST_FEATHER:
				aviaryHelper.loadAsync(data.getData());
				break;
			// default:
			// Session.getActiveSession().onActivityResult(this, requestCode,
			// resultCode, data);
			// break;
			}

			// checkInstagram();
		} else if (resultCode == RESULT_CANCELED) {
			switch (requestCode) {
			case ACTION_REQUEST_FEATHER:
				break;
			}
		}
		if (resultCode == ACTION_REQUEST_FACTORY
				&& requestCode == ACTION_REQUEST_FACTORY) {
			String jsonText = data.getStringExtra(Constants.Pref.SHOP_OBJECT);
			factory = gson.fromJson(jsonText, FactoryDao.class);
			doFactory(factory);
		}
		// mTwitter.onActivityResult(this, requestCode, resultCode, data);
		// fbHelper.onActivityResult(requestCode, resultCode, data);

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Dialog dialog = null;

		switch (id) {
		case EXTERNAL_STORAGE_UNAVAILABLE:
			dialog = new AlertDialog.Builder(this)
					.setTitle(R.string.external_storage_na_title)
					.setMessage(R.string.external_storage_na_message).create();
			break;
		case CHOOSE_IMAGE_METHOD:
			dialog = new AlertDialog.Builder(this)
					.setTitle("Add a photo to this check-in")
					.setItems(
							new String[] { "Use the Camera",
									"Pick from Gallery" },
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									switch (which) {
									case 0:
										takeFromCamera();
										break;
									case 1:
										pickFromGallery();
										break;
									default:
										break;
									}

								}
							}).create();
			break;

		}
		return dialog;

		// return super.onCreateDialog(id);
	}

	private void takeFromCamera() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, aviaryHelper.getImageUri());
		startActivityForResult(intent, ACTION_REQUEST_CAMERA);
	}

	/**
	 * Start the activity to pick an image from the user gallery
	 */
	private void pickFromGallery() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");

		Intent chooser = Intent.createChooser(intent, "Choose a Picture");
		startActivityForResult(chooser, ACTION_REQUEST_GALLERY);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.add(0, 1, 0, "Post")
				.setIcon(R.drawable.ic_action_send_now)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		return super.onCreateOptionsMenu(menu);
	}
}
