package com.shift.fashion;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.shift.fashion.ShopActivity.TabManager.TabInfo;
import com.shift.fashion.utils.Utils;

public class ShopActivity extends SherlockFragmentActivity {
	public TabHost mTabHost;
	TabManager mTabManager;
	public TabWidget tabWidget;
	private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
	public SlidingMenu menu;
	public ArrayList<String> StringId = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		menu = new SlidingMenu(this);

		menu.setMode(SlidingMenu.LEFT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setShadowDrawable(R.drawable.shadow_slide);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		menu.setMenu(R.layout.menu);
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
		tabWidget = (TabWidget) findViewById(android.R.id.tabs);

		// Toast.makeText(this, , Toast.LENGTH_SHORT).show();
		mTabManager = new TabManager(this, mTabHost, R.id.realtabcontent);
		StringId.add("Featured Store");
		StringId.add("Nearby Store");
		StringId.add("Profile");
		menu.toggle(true);
		mTabManager.addTab(
				mTabHost.newTabSpec(StringId.get(0)).setIndicator(
						buildIndicator(StringId.get(0),
								R.drawable.ic_tab_search)),
				FragmentStore.class, savedInstanceState);
		mTabManager.addTab(
				mTabHost.newTabSpec(StringId.get(1))
						.setIndicator(
								buildIndicator(StringId.get(1),
										R.drawable.ic_tab_near)),
				FragmentListNearBy.class, savedInstanceState);

		mTabManager.addTab(
				mTabHost.newTabSpec(StringId.get(2)).setIndicator(
						buildIndicator(StringId.get(2),
								R.drawable.ic_tab_profile)),
				ProfileFragment.class, savedInstanceState);
		// this.new TabManager(this, mTabHost, R.id.realtabcontent);

	}

	public void OnTabChangedFromSlider(String tabId) {
		mTabManager.onTabChanged(tabId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.add(0, 1, 0, "Add Review")
				.setIcon(R.drawable.write_review)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		menu.add(0, 2, 1, "Search")
				.setIcon(R.drawable.ic_action_search)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			menu.toggle();
			// finish();
			break;
		case 1:
			try {
				Utils.GetSavedLoginData(this);
				ReviewActivity.startThisActivity(this);

			} catch (Exception e) {
				mTabManager.onTabChanged(StringId.get(2));
				if (menu.isShown()) {
					menu.toggle();
				}
				Toast.makeText(this, "Please login First", Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case 2:
			SearchActivity.startThisActivity(this,
					ReviewActivity.ACTION_REQUEST_FACTORY + 1);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.shop, menu);
	// return true;
	// }
	@Override
	protected void onActivityResult(int RequestCode, int ResultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(RequestCode, ResultCode, data);
		if (RequestCode == 3) {
			ProfileFragment fragmentx = (ProfileFragment) mTabs.get(StringId
					.get(2)).fragment;
			fragmentx.OnActivityResultFromFragment(data);
		}

	}

	private View buildIndicator(String textRes, int drawable) {
		// if (textRes.equalsIgnoreCase("Checkin")) {
		// final View indicator = (View) getLayoutInflater().inflate(
		// R.drawable.tab_checkin, tabWidget, false);
		// indicator.findViewById(R.id.btn_tab_checkin).setOnClickListener(
		// new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// PostActivity.startThisActivity(
		// TorestoActivity.this, null);
		// }
		// });
		//
		// return indicator;
		// } else {
		final View indicator = getLayoutInflater().inflate(
				R.drawable.tab_main_indicator, tabWidget, false);
		TextView t = (TextView) indicator.findViewById(R.id.tab_main_label);
		ImageView i = (ImageView) indicator.findViewById(R.id.tab_main_image);
		i.setImageResource(drawable);
		t.setText(textRes);
		return indicator;
		// }
	}

	public class TabManager implements TabHost.OnTabChangeListener {
		private final FragmentActivity mActivity;
		private final TabHost mTabHost;
		private final int mContainerId;
		// private final HashMap<String, TabInfo> mTabs = new HashMap<String,
		// TabInfo>();
		TabInfo mLastTab;

		final class TabInfo {
			private final String tag;
			private final Class<?> clss;
			private final Bundle args;
			private Fragment fragment;

			TabInfo(String _tag, Class<?> _class, Bundle _args) {
				tag = _tag;
				clss = _class;
				args = _args;
			}
		}

		class DummyTabFactory implements TabHost.TabContentFactory {
			private final Context mContext;

			public DummyTabFactory(Context context) {
				mContext = context;
			}

			@Override
			public View createTabContent(String tag) {
				View v = new View(mContext);
				v.setMinimumWidth(0);
				v.setMinimumHeight(0);
				return v;
			}
		}

		public TabManager(FragmentActivity activity, TabHost tabHost,
				int containerId) {
			mActivity = activity;
			mTabHost = tabHost;
			mContainerId = containerId;
			mTabHost.setOnTabChangedListener(this);
		}

		public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
			tabSpec.setContent(new DummyTabFactory(mActivity));
			String tag = tabSpec.getTag();

			TabInfo info = new TabInfo(tag, clss, args);

			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state. If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			info.fragment = mActivity.getSupportFragmentManager()
					.findFragmentByTag(tag);
			if (info.fragment != null && !info.fragment.isDetached()) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager()
						.beginTransaction();
				// ft.detach(info.fragment);
				ft.hide(info.fragment);
				ft.commit();
			}

			mTabs.put(tag, info);
			mTabHost.addTab(tabSpec);
		}

		@Override
		public void onTabChanged(String tabId) {
			TabInfo newTab = mTabs.get(tabId);
			if (tabId.equalsIgnoreCase("profile")) {
				// if (!Utils.isLoggedIn(mActivity)) {
				// mActivity.startActivity(new Intent(mActivity,
				// LoginActivity.class));
				// mTabHost.setCurrentTabByTag(mLastTab.tag);
				// return;
				// }
			}
			if (mLastTab != newTab) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager()
						.beginTransaction();
				if (mLastTab != null) {
					if (mLastTab.fragment != null) {
						// ft.detach(mLastTab.fragment);
						ft.hide(mLastTab.fragment);
					}
				}

				if (newTab != null) {
					if (newTab.fragment == null) {
						newTab.fragment = Fragment.instantiate(mActivity,
								newTab.clss.getName(), newTab.args);
						ft.add(mContainerId, newTab.fragment, newTab.tag);
					} else {
						// ft.attach(newTab.fragment);
						ft.show(newTab.fragment);
					}
				}

				mLastTab = newTab;
				ft.commit();
				mActivity.getSupportFragmentManager()
						.executePendingTransactions();
			}
		}
	}
}
