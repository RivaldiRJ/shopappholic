package com.shift.fashion;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.androidquery.AQuery;
import com.google.myjson.Gson;
import com.markupartist.android.widget.PullToRefreshListView;
import com.markupartist.android.widget.PullToRefreshListView.OnRefreshListener;
import com.shift.fashion.adapter.ReviewAdapter;
import com.shift.fashion.api.LoginApi;
import com.shift.fashion.api.ReviewByUser;
import com.shift.fashion.dao.UserDao;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.Utils;

public class ProfileFragment extends SherlockFragment {
	private LoginApi api;
	private ProgressDialog dialog;

	public void OnActivityResultFromFragment(Intent data) {
		if (data != null) {
			Toast.makeText(getActivity(), "Please Login", Toast.LENGTH_SHORT)
					.show();
			final Gson gson = new Gson();
			UserDao dao = gson.fromJson(
					data.getStringExtra(Constants.Pref.USER_DATA),
					UserDao.class);
			Utils.SaveDataLogin(getActivity(), dao);
			doLogin(dao);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View v = inflater.inflate(R.layout.profile_layout, container,
				false);
		dialog = new ProgressDialog(getActivity());
		dialog.setCanceledOnTouchOutside(false);
		dialog.setMessage("Loging in....");
		dialog.setTitle("Shop App Holic");

		final EditText txtusername = (EditText) v
				.findViewById(R.id.editTextUsername);
		final EditText txtpassword = (EditText) v
				.findViewById(R.id.editTextPassword);
		api = new LoginApi(getActivity(), new LoginApi.ApiResultListener() {

			@Override
			public void onApiResultError(String errorMessage) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT)
						.show();
				dialog.dismiss();
			}

			@Override
			public void onApiPreCall() {
				// TODO Auto-generated method stub
				dialog.show();
			}

			@Override
			public void onApiResultOk(LoginApi.ApiDao data) {
				// TODO Auto-generated method stub
				final UserDao dao = data.getResult();
				Utils.SaveDataLogin(getActivity(), dao);
				doLogin(dao);
				dialog.dismiss();
			}
		});
		final Button buttonLogin = (Button) v.findViewById(R.id.buttonLogin);
		final Button buttonLogout = (Button) v.findViewById(R.id.buttonLogout);
		final Button buttonSignUp = (Button) v.findViewById(R.id.buttonSignUp);
		final LinearLayout layoutProfile = (LinearLayout) v
				.findViewById(R.id.layoutLoggedIn);
		final LinearLayout layoutLogin = (LinearLayout) v
				.findViewById(R.id.layoutLogin);

		buttonLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Utils.LogOut(getActivity());
				layoutLogin.setVisibility(View.VISIBLE);
				layoutProfile.setVisibility(View.GONE);
			}
		});
		buttonSignUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RegisterActivity.startThisActivity(getActivity());
			}
		});

		buttonLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				api.callApi(txtusername.getText() + "", txtpassword.getText()
						+ "");
			}
		});
		try {
			doLogin(Utils.GetSavedLoginData(getActivity()), v);
		} catch (Exception e) {

		}

		return v;
	}

	private void doLogin(UserDao data) {
		final View v = getView();
		doLogin(data, v);
	}

	private void doLogin(final UserDao data, final View v) {
		final TextView txtUsername = (TextView) v
				.findViewById(R.id.textViewUsername);
		final TextView txtFullName = (TextView) v
				.findViewById(R.id.textViewFullName);
		// final ImageView imageProfile = (ImageView)
		// v.findViewById(R.id.imagegl);
		final AQuery aq = new AQuery(getActivity());
		if (data.getImage() == null || !data.getImage().startsWith("https")
				|| !data.getImage().startsWith("http")) {
			aq.id(R.id.ImageProfile).image(R.drawable.ic_action_picture);
		} else {
			aq.id(R.id.ImageProfile).image(data.getImage());
		}
		final TextView txtEmail = (TextView) v.findViewById(R.id.textViewEmail);
		txtFullName.setText(data.getNama());
		txtEmail.setText(data.getEmail());
		txtUsername.setText(data.getUsername());
		final PullToRefreshListView lvReview = (PullToRefreshListView) v
				.findViewById(R.id.listViewReview);
		final LinearLayout layoutProfile = (LinearLayout) v
				.findViewById(R.id.layoutLoggedIn);
		final LinearLayout layoutLogin = (LinearLayout) v
				.findViewById(R.id.layoutLogin);
		layoutLogin.setVisibility(View.GONE);
		final ReviewByUser reviewApi = new ReviewByUser(getActivity(),
				new ReviewByUser.ApiResultListener() {

					@Override
					public void onApiResultError(String errorMessage) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), errorMessage,
								Toast.LENGTH_SHORT).show();
						lvReview.onRefreshComplete();

					}

					@Override
					public void onApiPreCall() {
						// TODO Auto-generated method stub

					}

					@Override
					public void onApiResultOk(ReviewByUser.ApiDao data) {
						// TODO Auto-generated method stub
						lvReview.onRefreshComplete();
						if (data.getResult().size() > 0) {
							ReviewAdapter adapter = new ReviewAdapter(
									getActivity(), data.getResult());
							lvReview.setAdapter(adapter);
						} else {
							ArrayAdapter<String> adapter = new ArrayAdapter<String>(
									getActivity(),
									android.R.layout.simple_list_item_1);
							adapter.add("No Review Has been made up to this day\nReview your favorite shop now :)");
							lvReview.setAdapter(adapter);
						}

					}
				});
		reviewApi.callApi(data.getId_user());
		lvReview.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				reviewApi.callApi(data.getId_user());
			}
		});
		// final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
		// getActivity(), android.R.layout.simple_list_item_1);
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		// adapter.add("test");
		layoutProfile.setVisibility(View.VISIBLE);
	}
}
