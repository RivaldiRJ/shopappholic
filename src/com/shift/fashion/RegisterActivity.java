package com.shift.fashion;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.myjson.Gson;
import com.shift.fashion.api.RegisterApi;
import com.shift.fashion.api.RegisterApi.ApiDao;
import com.shift.fashion.api.RegisterApi.ApiResultListener;
import com.shift.fashion.utils.AviaryHelper;
import com.shift.fashion.utils.Constants;
import com.shift.fashion.utils.ImageUtils;

import eu.janmuller.android.simplecropimage.CropImage;

public class RegisterActivity extends SherlockActivity {
	private static final int ACTION_REQUEST_CAMERA = 98;
	private static final int ACTION_REQUEST_GALLERY = 99;
	private static final int ACTION_REQUEST_FEATHER = 100;
	private static final int ACTION_REQUEST_CROP = 101;
	private static final int EXTERNAL_STORAGE_UNAVAILABLE = 1;
	private static final int CHOOSE_IMAGE_METHOD = 2;
	static final int DATE_DIALOG_ID = 20;
	private int mYear;
	private int mMonth;
	private int mDay;
	private String username, password, email, nama_asli, no_phone, birthday,
			url, about, sex;
	private TextView TextViewusername, TextViewpassword, TextViewemail,
			TextViewnama_asli, TextViewno_phone;
	private Bitmap bitmap;
	Uri imgUri;
	String imgPath;
	private RegisterApi api;
	private AviaryHelper aviaryHelper;
	private Button btnSelectDate, btnSelectPhoto;
	private ImageView imageProfile;
	private ProgressDialog dialog;
	private Spinner gender;

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDatePickerDisplay();
		}
	};
	private EditText confirmPassword;

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case ACTION_REQUEST_CAMERA:
				// String filepath = ImageUtils.getRealPath(this,
				// aviaryHelper.getImageUri());
				ImageUtils.startCropImage(this, aviaryHelper.getImageUri()
						.getPath(), ACTION_REQUEST_CROP);
				break;
			case ACTION_REQUEST_GALLERY:
				aviaryHelper.copyImageToTemp(this, data.getData());

				// filepath = ImageUtils.getRealPath(this,
				// aviaryHelper.getImageUri());
				ImageUtils.startCropImage(this, aviaryHelper.getImageUri()
						.getPath(), ACTION_REQUEST_CROP);
				break;
			case ACTION_REQUEST_CROP:
				File f = new File(data.getStringExtra(CropImage.IMAGE_PATH));
				imgUri = Uri.fromFile(f);
				imgPath = f.getPath();
				aviaryHelper.loadAsync(imgUri);
				break;

			case ACTION_REQUEST_FEATHER:
				aviaryHelper.loadAsync(data.getData());
				break;
			// default:
			// Session.getActiveSession().onActivityResult(this, requestCode,
			// resultCode, data);
			// break;
			}

			// checkInstagram();
		} else if (resultCode == RESULT_CANCELED) {
			switch (requestCode) {
			case ACTION_REQUEST_FEATHER:
				break;
			}
		}
		// mTwitter.onActivityResult(this, requestCode, resultCode, data);
		// fbHelper.onActivityResult(requestCode, resultCode, data);

	};

	private void updateDatePickerDisplay() {
		SimpleDateFormat sdf_format = new SimpleDateFormat("dd MM yyyy");
		final Calendar c = Calendar.getInstance();
		c.set(mYear, mMonth, mDay);
		btnSelectDate.setText(sdf_format.format(c.getTime()));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		TextViewusername = (TextView) findViewById(R.id.inp_username);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		TextViewpassword = (TextView) findViewById(R.id.inp_username);
		TextViewemail = (TextView) findViewById(R.id.inp_email);
		TextViewnama_asli = (TextView) findViewById(R.id.inp_name);
		TextViewno_phone = (TextView) findViewById(R.id.inp_phone);
		gender = (Spinner) findViewById(R.id.inp_gender);
		confirmPassword = (EditText) findViewById(R.id.inp_confirm_password);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.genders_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		gender.setAdapter(adapter);

		imageProfile = (ImageView) findViewById(R.id.profilePictureImageView);
		aviaryHelper = new AviaryHelper(this, imageProfile);
		dialog = new ProgressDialog(this);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setMessage("Registering....");
		dialog.setTitle("Shop App Holic");
		api = new RegisterApi(this, new ApiResultListener() {

			@Override
			public void onApiResultError(String errorMessage) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), errorMessage,
						Toast.LENGTH_SHORT).show();
				dialog.cancel();
			}

			@Override
			public void onApiPreCall() {
				// TODO Auto-generated method stub
				dialog.show();
			}

			@Override
			public void onApiResultOk(ApiDao data) {
				// TODO Auto-generated method stub
				dialog.cancel();
				final Intent intent = new Intent();
				final Gson gson = new Gson();
				intent.putExtra(Constants.Pref.USER_DATA,
						gson.toJson(data.getResult()));
				setResult(1, intent);
				finish();
			}
		});
		btnSelectDate = (Button) findViewById(R.id.inp_birthday);
		dialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface arg0) {
				// TODO Auto-generated method stub
				api.cancelAPI();
			}
		});
		btnSelectPhoto = (Button) findViewById(R.id.pictureButton);
		btnSelectPhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog(CHOOSE_IMAGE_METHOD);
			}
		});
		btnSelectDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog(DATE_DIALOG_ID);
			}
		});

	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Dialog dialog = null;

		switch (id) {
		case DATE_DIALOG_ID:
			dialog = new DatePickerDialog(this, mDateSetListener, mYear,
					mMonth, mDay);
			break;
		case EXTERNAL_STORAGE_UNAVAILABLE:
			dialog = new AlertDialog.Builder(this)
					.setTitle(R.string.external_storage_na_title)
					.setMessage(R.string.external_storage_na_message).create();
			break;
		case CHOOSE_IMAGE_METHOD:
			dialog = new AlertDialog.Builder(this)
					.setTitle("Add a photo to this check-in")
					.setItems(
							new String[] { "Use the Camera",
									"Pick from Gallery" },
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									switch (which) {
									case 0:
										takeFromCamera();
										break;
									case 1:
										pickFromGallery();
										break;
									default:
										break;
									}

								}
							}).create();
			break;

		}
		return dialog;
	}

	private void takeFromCamera() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, aviaryHelper.getImageUri());
		startActivityForResult(intent, ACTION_REQUEST_CAMERA);
	}

	/**
	 * Start the activity to pick an image from the user gallery
	 */
	private void pickFromGallery() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");

		Intent chooser = Intent.createChooser(intent, "Choose a Picture");
		startActivityForResult(chooser, ACTION_REQUEST_GALLERY);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			// menu.toggle();
			finish();
			break;
		case 1:
			if (gender.getSelectedItemPosition() > 0) {
				sex = "1";
			} else {
				sex = "0";
			}
			api.callAPI(TextViewusername.getText().toString(), TextViewpassword
					.getText().toString(), TextViewemail.getText().toString(),
					TextViewnama_asli.getText().toString(), TextViewno_phone
							.getText().toString(), mDay + "-" + mMonth + "-"
							+ mYear, "", "", sex, aviaryHelper.getBitmap());
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		menu.add(0, 1, 0, "Register")
				.setIcon(R.drawable.ic_action_send_now)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_WITH_TEXT);

		return super.onCreateOptionsMenu(menu);
	}

	public static void startThisActivity(Activity ctx) {
		Intent intent = new Intent(ctx, RegisterActivity.class);
		ctx.startActivityForResult(intent, 3);
	}

}
